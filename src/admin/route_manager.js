import React, { Suspense } from "react";
import { Switch, Route } from "react-router-dom";

//////////////component///////////////////////////////////
const Annonce = React.lazy(() => import("./components/annonce"));
const User = React.lazy(() => import("./components/user"));
/////////////////////////////////////////////////////////

export default function RouterMananger() {
    return (
        <Suspense fallback={null}>
            <Switch>
                <Route path="/admin" exact component={Annonce} />
                <Route path="/admin/user" exact component={User} />
            </Switch>
        </Suspense >
    )
}