import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import "../assets/css/style.css";
import "../assets/icon/index";
import AvatarDefault from "../assets/icon/avatar_default1.png";

import Content from "./route_manager";

export default function Admin() {

    const closeSideNav = () => {
        document.getElementById("mySidebar").style.display = "none";
        document.getElementById("myOverlay").style.display = "none";
    }

    const openSideNav = () => {
        document.getElementById("mySidebar").style.display = "block";
        document.getElementById("myOverlay").style.display = "block";
    }

    return (
        <div className="w3-content" style={{ maxWidth: 1200 }}>
            <nav className="w3-sidebar w3-bar-block w3-white w3-collapse w3-top" style={{ zIndex: 3, width: 250 }} id="mySidebar">
                <div className="w3-container w3-display-container w3-padding-16">
                    <div onClick={() => closeSideNav()} className="w3-hide-large w3-button w3-display-topright"><FontAwesomeIcon icon="times" /></div>
                    <h3 className="w3-wide"><b>Admin</b></h3>
                </div>
                <div className="w3-padding-64 w3-large w3-text-grey" style={{ fontWeight: "bold" }}>
                    <div onClick={() => window.location.assign("/admin/annonce")} className="w3-bar-item w3-button"><FontAwesomeIcon icon="bullhorn" />&nbsp;&nbsp;Annonce</div>
                    <div onClick={() => window.location.assign("/admin/produit")} className="w3-bar-item w3-button"><FontAwesomeIcon icon="boxes" />&nbsp;&nbsp;Produit</div>
                    <div onClick={() => window.location.assign("/admin/user")} className="w3-bar-item w3-button"><FontAwesomeIcon icon="users-cog" />&nbsp;&nbsp;Utilisateur</div>
                </div>
            </nav>
            <header className="w3-bar w3-top w3-hide-large w3-dark-gray w3-xlarge">
                <div className="w3-bar-item w3-padding-24 w3-wide">Admin</div>
                <div onClick={() => openSideNav()} className="w3-bar-item w3-button w3-padding-24 w3-right"><FontAwesomeIcon icon="bars" /></div>
            </header>

            <div className="w3-overlay w3-hide-large"  style={{cursor: "pointer"}} id="myOverlay"></div>
            
            <div className="w3-main" style={{marginLeft: 250}}>
                {/* Push down content on small screens  */}
                <div className="w3-hide-large" style={{marginTop:100}}></div>
                <div className="w3-row" style={{marginTop: 36, marginRight: "2%"}}>
                    <div className="w3-right">
                        <img src={AvatarDefault} alt="avaytar" style={{height: 60, width: 60, borderRadius: "100%"}} />
                    </div>
                </div>
                <div className="contentRoute" style={{width: "100%", padding: "2%"}}>
                    <Content />
                </div>
            </div>
        </div>
    );
} 