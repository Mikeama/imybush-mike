import React, { useState } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import "../css/admin.css";

const UserList = () => {
    return (
        <div className="userListContainer">
            <h2>Liste des utlisateurs de <b>Inmybush</b></h2>
            <div className="w3-row" style={{ marginTop: 24, marginBottom: 24 }}>
                <div className="w3-right" style={{ minWidth: 250 }}>
                    <input type="text" id="searchUser" className="form-control" placeholder="Recherche..." />
                </div>
            </div>
            <div className="table-responsive">
                <table className="table userList table-bordered">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Nom d'utilisateur</th>
                            <th>Nom et Prenom</th>
                            <th>Email</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>

                </table>
            </div>
        </div>
    )
}

const AdminList = () => {
    return (
        <div className="adminList">
            <h2>Liste des administrateurs</h2>
            <div className="table-responsive">
                <div className="w3-row" style={{marginTop: 24, marginBottom: 36}}>
                    <button className="adminBtn c-green"><FontAwesomeIcon icon="plus-circle"/>&nbsp;Ajouter un administrateur</button>
                </div>
                <table className="table userList table-bordered">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Nom et Prenom</th>
                            <th>Email</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>

                </table>
            </div>
        </div>
    )
}

export default function User() {

    const [varContent, setVarContent] = useState(<UserList />)

    const showComponent = (Component) => {
        setVarContent(Component);
    }
    return (
        <div className="userManagerContainer">
            <h1>Gestion des utilisateurs du systeme</h1>
            <div className="w3-content" style={{ marginTop: 36 }}>
                <button onClick={() => showComponent(<UserList />)} className="adminBtn c-green">Utilisateur</button>&nbsp;&nbsp;
                <button onClick={() =>showComponent(<AdminList />)} className="adminBtn c-green">Administrateur</button>
                <div className="varContent" style={{ marginTop: 36 }}>
                    {varContent}
                </div>
            </div>
        </div>
    );
}