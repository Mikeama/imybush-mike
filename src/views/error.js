import React from "react";
import { Link } from "react-router-dom"

export default class Error extends React.Component {
    render() {
        return (
            <div style={{
                marginBottom: 100
            }} className="errorContainer container">
                <div class="erreurContent">
                    <h1 style={{
                        fontSize: 60,
                        color: "red",
                        fontWeight: "bold"
                    }}>ERREUR 404 !!!</h1>
                    <h2 style={{
                        fontSize: 50
                    }}>Cet URL n'existe pas.</h2>
                    <Link to={"/"} style={{
                        height: 40,
                        color: "blue",
                        fontSize: 20,
                        textDecoration: "underline"
                    }}>Veuillez revenir dans la page d'acceuil</Link>
                </div>
            </div>
        );
    }
}