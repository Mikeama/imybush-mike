import React from "react";

import Footer from "./layout/footer/footer";
import Content from "./route_manager";
import Header from "./layout/header/header";

const Index = () => {
    return (
        <>
            <header>
                <Header />
            </header>
            <section id="mysection">
                <Content />
            </section>
            <footer>
                <Footer />
            </footer>
        </>
    );
}

export default Index;