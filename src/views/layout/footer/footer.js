import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import "./footer.css";
import logo from "./logo.png";

class Footer extends React.Component {
    render() {
        return (
            <div id="footerContainer" >
                <div className="container">
                    <div className="row" style={{marginBottom: 25}}>
                        <div className="col-lg-3 col-md-3 col-sm-12">
                            <h3>MON COMPTE<div className="underline"></div></h3>
                            <div className="link">Se Connecter</div>
                            <div className="link">A propos de nous</div>
                            <div className="link">Nous contacter</div>
                        </div>
                        <div className="col-lg-5 col-md-5 col-sm-12">
                            <h3>CATEGORIES<div className="underline"></div></h3>
                            <div className="row">
                                <div className="col-sm-4">
                                    <div className="link">Fruits</div>
                                    <div className="link">Légumes</div>
                                    <div className="link">Viandes</div>
                                    <div className="link">Plantes graines</div>
                                </div>
                                <div className="col-sm-4">
                                    <div className="link">Condiments et épices</div>
                                    <div className="link">Poissons et fruit de mer</div>
                                    <div className="link">Oeufs</div>
                                    <div className="link">Bois</div>
                                </div>
                                <div className="col-sm-4">
                                    <div className="link">Paniers</div>
                                    <div className="link">Produits Transformés</div>
                                    <div className="link">Animaux</div>
                                    <div className="link">Plantes médicinales</div>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-4 col-md-4 col-sm-12">
                            <h3>NEWSLETTER<div className="underline"></div></h3>
                            <form>
                                <input type="text" id="emailTextField" placeholder="Votre adresse mail " />&nbsp;&nbsp;
                                <input type="submit" style={{ minWidth: 120, marginBottom: 10 }} className="my-btn c-green" value="Envoyer" />
                            </form>
                        </div>
                    </div>
                    <div className="row" style={{marginBottom: 25}}>
                        <div className="col-lg-3 col-md-3 col-sm-6">
                            <img src={logo} width="90px" height="65px" alt="logo" />
                            <h4>Contacts</h4>
                            <div className="link"><FontAwesomeIcon icon="map-marker" />&nbsp;&nbsp;Localisation</div>
                            <div className="link"><FontAwesomeIcon icon="phone" />&nbsp;&nbsp;(+33) 34 344 12</div>
                            <div className="link"><FontAwesomeIcon icon="envelope" />&nbsp;&nbsp;inmybush@inmybush.fr</div>
                        </div>
                        <div className="col-lg-5 col-md-5 col-sm-12">
                            <h4>A propos</h4>
                            <div className="link">
                                <p style={{ textAlign: "justify" }}>We have created a fictional band website. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
                            </div>
                        </div>
                        <div className="col-lg-3 col-md-3 col-sm-12">
                            <h3>RESTEZ CONNECTE<div className="underline"></div></h3>
                            <div className="link">Facebook</div>
                            <div className="link">Twitter</div>
                            <div className="link">Linked In</div>
                        </div>
                    </div>
                </div>
                <div id="end">
                    <div id="copyright">Dans mon jardin © 2020</div>
                </div>
            </div>
        );
    }
}
export default Footer;