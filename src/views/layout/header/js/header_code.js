import $ from "jquery";

const ReponsiveHeader = () => {
    $(document).ready(() => {
        responsive();
        $(window).resize(() => { 
            responsive(); 
            if(($("#searchContainer").offset())) {
                $("#my-header-front").css("height", ($("#searchContainer").offset().top + 130) + "px"); 
            }
        });
        $("header").css("height", ($("#searchContainer").offset().top + 130) + "px");
        function responsive() {
            let rightLogin = ($(window).width() - $("#myNav").width()) / 2;
            $("#loginContainer").css('right', rightLogin + 'px');
            if ($(window).width() <= 1199) {
                $("#searchContainer").css({
                    "position": "absolute",
                    "right": rightLogin + 'px',
                    "top": "75px",
                    "width": "200px"
                });
            } else {
                $("#serchLogin").css('width', "400px");
                $("#searchContainer").css({
                    "position": "initial",
                });
            }
    
            if ($(window).width() <= 604) {
                $("#btnFindContainer").css("margin-top", "10px");
                $("#searchContainer").css({
                    "position": "initial",
                    "margin-top": "20px"
                });
            } else {
                $("#btnFindContainer").css("margin-top", "40px");
                $("#searchContainer").css({
                    "margin-top": "40px"
                });
            }
    
            if($(window).width() <= 450) {
                $("#find").css("min-width", "350px");
            } else {    
                $("#find").css("min-width", "450px");
            }
    
            if ($(window).width() <= 974) {
                $("#mysection").css("margin-top", "355px");
            } else if ($(window).width() > 975 && $(window).width() <= 993) {
                $("#mysection").css("margin-top", "190px");
            } else if ($(window).width() > 993 && $(window).width() <= 1182) {
                $("#mysection").css("margin-top", "190px");
            } else {
                $("#mysection").css("margin-top", "190px");
            }
        }
    })
    
}
export default ReponsiveHeader;


