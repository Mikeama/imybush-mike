import React, { Component } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import filterListDropdown from "../../../../assets/js/filterDropdown";
import regions from "./data/region";
import categories from "./data/categorie";
import villes from "./data/villes";
import "./filter.css";
import "../../../../assets/icon/index";

class Filter extends Component {

    state = {
        typeData: [{ name: "PRODUIT1" }, { name: "PRODUIT2" },{ name: "PRODUIT3" }, { name: "PRODUIT4" },{ name: "PRODUIT5" }, { name: "PRODUIT6" }],
        regionData: regions,
        villeData: villes,
        categorieData: categories,
        regionFilter: {
            placeholder: "ARRONDISSEMENTS",
            value: null
        },
        villeFilter: {
            placeholder: "VILLES",
            value: null
        },
        categorieFilter: {
            placeholder: "CATEGORIES",
            value: null
        },
        typeFilter: {
            placeholder: "PRODUIT",
            value: "produit"
        }
    };

    ///////////////////////REGION_FILTER////////////////////
    regionChoosen = (value, name, idList) => {
        this.setState({
            regionFilter: {
                placeholder: name,
                value: value
            }
        });
        filterListDropdown.showList(idList);
    }
    ///////////////////////////////////////////////////////

    ///////////////////VILLES_FILTER//////////////////////
    villeChoosen = (value, name, idList) => {
        this.setState({
            villeFilter: {
                placeholder: name,
                value: value
            }
        });
        filterListDropdown.showList(idList); // to hidden droplist
    }
    /////////////////////////////////////////////////////

    /////////////////VILLE_CATEGOTIE////////////////////
    categorieChoosen = (value, name, idList) => {
        this.setState({
            categorieFilter: {
                placeholder: name,
                value: value
            }
        });
        filterListDropdown.showList(idList); // to hidden droplist
    }
    ///////////////////////////////////////////////////

    //////////////////type_filter///////////////////////
    typeChoosen = (value, name, idList) => {
        this.setState({
            typeFilter: {
                placeholder: name,
                value: value
            }
        });
        filterListDropdown.showList(idList); // to hidden droplist
    }
    ////////////////////////////////////////////////////

    render() {
        return (
            <div className="w3-container">
                <div className="row my-row" id="filter">
                    <div className="col-lg-3 col-md-3 col-sm-12 my-col">
                        <div className="dropdown">
                            <button onClick={() => filterListDropdown.showList("regionList")} className="dropbtnFilter">
                                <div className="placeholder">{this.state.regionFilter.placeholder}</div>
                                <div className="chevronDown"><FontAwesomeIcon icon="chevron-down" /></div>
                            </button>
                            <div id="regionList" className="dropdown-content">
                                {/* <input type="text" placeholder="Recherche..." id="regionInput" className="myInput" onKeyUp={() => filterListDropdown.filterList("regionInput", "regionList")} /> */}
                                <div className="list-content">
                                    <a onClick={() => this.regionChoosen(null, "ARRONDISSEMENTS", "regionList")} href={"#_REGION"}>ARRONDISSEMENTS&nbsp;<span style={{ fontSize: 9, color: "grey" }}>(par défaut)</span></a>
                                    {
                                        this.state.regionData.map((region, key) => (
                                            <a key={key} onClick={() => this.regionChoosen(region.id, region.name, "regionList")} href={"#_" + region.name}>{region.name}</a>
                                        ))
                                    }
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-lg-3 col-md-3 col-sm-12 my-col">
                        <div className="dropdown">
                            <button onClick={() => filterListDropdown.showList("villeList")} className="dropbtnFilter">
                                <div className="placeholder">{this.state.villeFilter.placeholder}</div>
                                <div className="chevronDown"><FontAwesomeIcon icon="chevron-down" /></div>
                            </button>
                            <div id="villeList" className="dropdown-content">
                                <input type="text" placeholder="Recherche..." id="villeInput" className="myInput" onKeyUp={() => filterListDropdown.filterList("villeInput", "villeList")} />
                                <div className="list-content">
                                    <a onClick={() => this.villeChoosen(null, "VILLES", "villeList")} href={"#_VILLES"}>VILLES&nbsp;<span style={{ fontSize: 9, color: "grey" }}>(par défaut)</span></a>
                                    {
                                        this.state.villeData.map((ville, key) => (
                                            <a key={key} onClick={() => this.villeChoosen(ville.code, ville.name, "villeList")} href={"#_" + ville.name}>{ville.name}({ville.code})</a>
                                        ))
                                    }
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-lg-3 col-md-3 col-sm-12 my-col">
                        <div className="dropdown">
                            <button onClick={() => filterListDropdown.showList("categorieList")} className="dropbtnFilter">
                                <div className="placeholder">{this.state.categorieFilter.placeholder}</div>
                                <div className="chevronDown"><FontAwesomeIcon icon="chevron-down" /></div>
                            </button>
                            <div id="categorieList" className="dropdown-content">
                                <input type="text" placeholder="Recherche..." id="categorieInput" className="myInput" onKeyUp={() => filterListDropdown.filterList("categorieInput", "categorieList")} />
                                <div className="list-content">
                                    <a onClick={() => this.categorieChoosen(null, "CATEGORIES", "categorieList")} href={"#_CATEGORIES"}>CATEGORIES&nbsp;<span style={{ fontSize: 9, color: "grey" }}>(par défaut)</span></a>
                                    {
                                        this.state.categorieData.map((categorie, key) => (
                                            <a key={key} onClick={() => this.categorieChoosen(categorie.id, categorie.name, "categorieList")} href={"#_" + categorie.name}>{categorie.name}</a>
                                        ))
                                    }
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-lg-3 col-md-3 col-sm-12 my-col">
                        <div className="dropdown">
                            <button onClick={() => filterListDropdown.showList("typeList")} className="dropbtnFilter">
                                <div className="placeholder">{this.state.typeFilter.placeholder}</div>
                                <div className="chevronDown"><FontAwesomeIcon icon="chevron-down" /></div>
                            </button>
                            <div id="typeList" className="dropdown-content">
                                <input type="text" placeholder="Recherche..." id="typeInput" className="myInput" onKeyUp={() => filterListDropdown.filterList("typeInput", "typeList")} />
                                <div className="list-content">
                                    {
                                        this.state.typeData.map((type, key) => (
                                            <a key={key} onClick={() => this.typeChoosen(type.name, type.name, "typeList")} href={"#_" + type.name}>{type.name}</a>
                                        ))
                                    }
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-lg-12 col-md-12 col-sm-12 my-col">
                        <label className="switch">
                            <input type="checkbox" />
                            <span className="slider round"></span>
                        </label>&nbsp;
                    <span>Voir uniquement les produits en stock</span>
                    </div>
                    <div className="col-lg-12 col-md-12 col-sm-12 my-col">
                        <div id="btnSearchContainer">
                            <button className="my-btnfilter c-green">Rechecher</button>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default Filter;