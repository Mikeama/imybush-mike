var regions = [
  {
    code: 9711,
    name: "Basse-Terre"
  },
  {
    code: 9712,
    name: "Pointe-à-Pitre"
  }
]

export default regions;