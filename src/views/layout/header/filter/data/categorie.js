var categories = [
	{
		id: 1,
		name: "Fruits"
	},
	{
		id: 2,
		name: "Légumes"
	},
	{
		id: 3,
		name: "Viandes"
	},
	{
		id: 4,
		name: "Plantes graines"
	},
	{
		id: 5,
		name: "Condiments et épices"
	},
	{
		id: 6,
		name: "Poissons et fruit de mer"
	},
	{
		id: 7,
		name: "Oeufs"
	},
	{
		id: 8,
		name: "Bois"
	},
	{
		id: 9,
		name: "Paniers"
	},
	{
		id: 10,
		name: "Produits Transformés"
	},
	{
		id: 11,
		name: "Animaux"
	},
	{
		id: 12,
		name: "Plantes médicinales"
	}
]
export default categories