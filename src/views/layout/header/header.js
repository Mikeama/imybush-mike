import React, { useEffect, useState } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Filter from "./filter/filter";
import ResponiveHeader from "./js/header_code";
import "./css/header.css";
import logo from "./img/logo.png";
import { useSelector, useDispatch } from "react-redux";
import avartar from "../../../assets/icon/avatar_default1.png";
import { Popover, Whisper } from 'rsuite';
import { loggoutUser } from "../../../my_redux/actions/logginUser";
import $ from "jquery";
import Axios from "axios";
import { useAlert } from "react-alert";

const url = process.env.REACT_APP_BACKEND_URL;

const Speaker = ({ content, ...props }) => {

    const dispatch = useDispatch();

    return (
        <Popover  {...props}>
            <div className="name">{props.name}</div>
            <div className="username">@{props.username}</div>
            <hr />
            <div onClick={() => { window.location.assign("/front/mon_compte") }} className="itemProfil"><FontAwesomeIcon icon='user' />&nbsp;Mon compte</div>
            <div onClick={() => dispatch(loggoutUser())} className="itemProfil"><FontAwesomeIcon icon='sign-out-alt' />&nbsp;Déconnexion</div>
        </Popover>
    );
};

export default function Header() {

    const user = useSelector(state => state.logger);
    const [loginContainer, setLoginContainer] = useState(null);
    const [elementTop, setElementTop] = useState("");
    const [currentUser, setCurrentUser] = useState({});
    const alert = useAlert();

    useEffect(() => {
        $(document).ready(() => {
            placement();
            $(window).resize(() => {
                placement();
            });
            function placement() {
                var windowWidth = $(window).width();
                if (windowWidth > 1300) {
                    setElementTop("bottom");

                } else {
                    setElementTop("leftStart");
                }
            }
        });

        ResponiveHeader();

        Axios.get(url + '/user/' + user.current_user.id)
            .then(res => {
                setCurrentUser(res.data)
            })
            .catch(error => {
                alert.error(error.message);
            })

        // change the avatar when user is logged
        if (user) {
            setLoginContainer(
                <>
                    <Whisper
                        trigger="click"
                        enterable
                        placement={elementTop}
                        speaker={<Speaker name={user.current_user.name} username={user.current_user.username} content={"null"} />}
                    >
                        <div className="userAvatar" style={{ marginTop: -7 }}>
                            <img src={currentUser.photo ? url + "/images/" + currentUser.photo : avartar} className="img-circle" style={{ width: 50, height: 50, objectFit:"cover", objectPosition:"center"}} alt="logo" />
                        </div>
                    </Whisper>
                </>
            );
        } else {
            setLoginContainer(
                <button id="loginBtn" onClick={() => { window.location.assign("/front/login"); }} className="my-btn c-green">Se connecter</button>
            );
        }
    }, [alert, currentUser.photo, elementTop, user]);

    return (
        <div>
            <header id="my-header-front">
                <br />
                <div className="container" id="myNav">
                    <div className="flex-container" id="flex">
                        <div id="logo">
                            <img src={logo} onClick={() => window.location.assign("/")} style={{
                                width: 125,
                                height: 90,
                                cursor: "pointer"
                            }} alt="logo" />
                        </div>
                        <div id="find">
                            <div id="btnFindContainer">
                                <button onClick={() => { window.location.assign("/front/trouver_produit"); }} className="c-orange my-btn">Trouver un produit</button>&nbsp;&nbsp;
                                    <button onClick={() => { window.location.assign("/front/proposer_produit"); }} className="c-orange my-btn">Proposer un produit</button>
                            </div>
                        </div>
                        <div id="serchLogin" className="flex-container">
                            <div id="serchLoginContainer">
                                <div className="flex-container">
                                    <div id="searchContainer">
                                        <div className="flex-container">
                                            <div id="searchInput">
                                                <input id="searchInputElement" type="text" placeholder="Recherche" />
                                            </div>
                                            <div id="searchIcon" className="c-orange">
                                                <FontAwesomeIcon id="iconElement" icon="search" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="loginContainer">
                    {loginContainer}
                </div>
            </header>
            <div className="container">
                <Filter />
            </div>
        </div>
    );
}
