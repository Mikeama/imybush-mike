import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Flickity from "flickity";
import "../css/flikityComp.css";
import "../../../assets/css/flickity.min.css"
import "../css/new.css";
import placehoder from "./placleholder.png";
import produitData from "./data/newAnnonce";
import CardProduit from "../../../components/cardProduit/carProduit";

class New extends React.Component {

    flickity = null;

    state = {
        produits: produitData,
        cell: 2
    };

    initFlickity = () => {
        const elem = document.querySelector('.grid-slider-wrap_new');
        const flickity = new Flickity(elem, {
            cellAlign: 'left',
            contain: true,
            freeScroll: true,
            prevNextButtons: false,
            pageDots: false,
            groupCells: "100%",
            lazyLoad: true

        });
        this.flickity = flickity;
    }

    componentDidMount() {
        this.initFlickity();
    }


    render() {
        return (
            <div>
                <div id="section">
                    <h3 style={{ marginBottom: 20, marginLeft: "0.5%",fontWeight: 500 }}><span style={{ color: "rgb(12, 168, 25)" }}>NOUVEAUX</span> ANNONCES</h3>
                    <div className="grid-slider-wrap_new">
                        {
                            this.state.produits.map((produit, key) => (
                                <div key={key} className="gride-slide">
                                    <div className="grid-slide-inner">
                                        <CardProduit imageOrPlaceholder={key <= 5 ? produit.photo : placehoder } altImage={produit.name} imageLazyload={produit.photo} nameProduit={produit.name} prixProduit={produit.prix} />
                                    </div>
                                </div>
                            ))
                        }
                    </div>
                </div>
                <div className="w3-display-container" >
                    <div className="w3-display-right" id="btnPrevNextContainer">
                        <button onClick={() => {
                            this.flickity.previous();
                        }} className="my-btn-new"><FontAwesomeIcon icon="angle-left" /></button>
                        &nbsp;&nbsp;
                       <button onClick={() => {
                            this.flickity.next();
                        }} className="my-btn-new"><FontAwesomeIcon icon="angle-right" /></button>
                    </div>
                </div>
            </div>
        );
    }
}

export default New;


