var favoriAnnonces = [
  {
    name: "Pomme de terre",
    prix: "12",
    photo: "/image/pomme_de_terre.jpg"
  },
  {
    name: "Citron",
    prix: "13",
    photo: "/image/citron.jpg"
  },
  {
    name: "Brocolli",
    prix: "14",
    photo: "/image/brocolli.jpg"
  },
  {
    name: "Asperges",
    prix: "14",
    photo: "/image/asperge.jpg"
  },
  {
    name: "Kiwi",
    prix: "14",
    photo: "/image/kiwi.jpg"
  },
  {
    name: "Avocat",
    prix: "15",
    photo: "/image/avocat.jpg"
  },
  {
    name: "Tomate",
    prix: "13",
    photo: "/image/tomate.jpg"
  },
  {
    name: "Carotte",
    prix: "10",
    photo: "/image/carotte.jpg"
  },
  {
    name: "Haricot vert",
    prix: "12",
    photo: "/image/harico_vert.jpg"
  },
  {
    name: "Viande hachée",
    prix: "12",
    photo: "/image/viande_hachee.jpg"
  },
  {
    name: "Orange",
    prix: "09",
    photo: "/image/orange.jpg"
  },
  {
    name: "Melon",
    prix: "10",
    photo: "/image/melon.jpg"
  },
  {
    name: "Fraise",
    prix: "06",
    photo: "/image/fraise.jpg"
  },
  {
    name: "Filet de poisson",
    prix: "15",
    photo: "/image/filet_de_poisson.jpg"
  },
  {
    name: "Pomme",
    prix: "15",
    photo: "/image/pomme.jpg"
  }
];

export default favoriAnnonces;
