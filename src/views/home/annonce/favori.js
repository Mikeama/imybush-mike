import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Flickity from "flickity";
import "../css/flikityComp.css";
import "../../../assets/css/flickity.min.css"
import placehoder from "./placleholder.png";
import "../css/favori.css";
import favorieAnnonces from "./data/favoriAnnonce";
import CardProduit from "../../../components/cardProduit/carProduit";

class Favori extends React.Component {

    flickity = null;

    state = {
        produits: favorieAnnonces
    };

    initFlickity = () => {
        const elem = document.querySelector('.grid-slider-wrap');
        const flickity = new Flickity(elem, {
            cellAlign: 'left',
            contain: true,
            freeScroll: true,
            prevNextButtons: false,
            pageDots: false,
            groupCells: "100%",
            lazyLoad: true

        });
        this.flickity = flickity;
    }

    componentDidMount() {
        this.initFlickity()
    }

    render() {
        return (
            <div>
                <div id="favoriContent">
                    <div id="favoriheader">
                        <div className="w3-display-container">
                            <div className="w3-display-left"><h3 style={{fontWeight: 500}}><span style={{ color: "rgb(12, 168, 25)" }}> ANNONCES</span></h3></div>
                            <div className="w3-display-right">
                                <button onClick={() => {
                                    this.flickity.previous()
                                }} className="my-btn-favori"><FontAwesomeIcon icon="angle-left" /></button>
                            &nbsp;&nbsp;
                            <button onClick={() => {
                                    this.flickity.next();
                                }} className="my-btn-favori"><FontAwesomeIcon icon="angle-right" /></button>
                            </div>
                        </div>
                    </div>
                    <div className="grid-slider-wrap" id="cardFavori">
                        {
                            this.state.produits.map((produit, key) => (
                                <div key={key} className="gride-slide">
                                    <div className="grid-slide-inner">
                                    <CardProduit imageOrPlaceholder={key <= 5 ? produit.photo : placehoder } altImage={produit.name} imageLazyload={produit.photo} nameProduit={produit.name} prixProduit={produit.prix} />
                                    </div>
                                </div>
                            ))
                        }
                    </div>
                </div>

            </div>
        );
    }
}

export default Favori;