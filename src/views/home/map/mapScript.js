import $ from "jquery";

var element = document.getElementsByClassName("map");
var elementClicked = null;
$(document).ready(() => {
  initFill();
  for (var i = 0; i < element.length; i++) {
    // eslint-disable-next-line no-loop-func
    element[i].addEventListener("click", (event) => {
      var element = event.target;
      elementClicked = element;
      initFill();
    });
    element[i].addEventListener("mouseover", (event) => {
      var element = event.target;
      initFill();
      element.setAttribute('fill', 'rgb(12, 168, 25)');
    });
    element[i].addEventListener("mouseout", (event)=> {
      initFill();
    })
  }
});

function initFill() {
  for (var i = 0; i < element.length; i++) {
    element[i].setAttribute('fill', 'rgba(248, 133, 1, 0.5)');
  }
  if(elementClicked) {
    elementClicked.setAttribute('fill', 'rgb(12, 168, 25)');
  }
}


