import React, { Component } from "react";
import InfiniteScroll from "react-infinite-scroll-component";
import $ from "jquery";
import Vendeur from "../../components/vendeurList/vendeurList";
import New from "./annonce/new";
import Favori from "./annonce/favori";
import "./css/home.css";
import vendeurs from "./data/vendeur";

class Home extends Component {
    state = {
        vendeurs: vendeurs[17].vendeurs,
    }

    mapOnclick = (code) => {
        $(".loader").fadeIn();
        setTimeout(() => {
            this.setState({
                vendeurs: vendeurs[Math.floor(Math.random() * (vendeurs.length - 1))].vendeurs
            });
            $(".loader").fadeOut();
        }, 2000);
    }

    componentDidMount() {
        $(document).ready(() => {

            //set map content
            $("#mapContent").html($("#carte_guadeloupe").html());

            var element = $(".map");
            initFill();
            var elementClicked = null;
            var nameMap = "";

            $(".map").click((event) => {
                initFill();
                elementClicked = event.currentTarget;
                elementClicked.style.fill = "rgb(248, 100, 1)";
                var data = elementClicked.getAttribute('data').split("_");
                $("#nameCommune").html(data[1]);
                nameMap = data[1]; // nameMap
                this.mapOnclick(data[0]); // code
            });

            $(".map").mouseover((event) => {
                if (event.currentTarget !== elementClicked) {
                    event.currentTarget.style.fill = "rgb(248, 100, 1)";
                }
                $("#nameCommune").html(event.currentTarget.getAttribute('data').split("_")[1]);
            });

            $(".map").mouseout((event) => {
                if (event.currentTarget !== elementClicked) {
                    event.currentTarget.style.fill = "rgba(248, 133, 1, 0.5)";
                }
                $("#nameCommune").html(nameMap);
            })

            function initFill() {
                for (var i = 0; i < element.length; i++) {
                    element[i].style.fill = "rgba(248, 133, 1, 0.5)";
                }
            }
        });
    }

    render() {
        return (
            <div id="home">
                <div className="container">
                    <div id="VendeurAndmap">
                        <div className="row">
                            <div className="col-lg-7 col-md-7 col-sm-12" id="mapContent" style={{ marginBottom: 15 }}>
                                {/* map content */}
                            </div>
                            <div className="col-lg-5 col-md-5 col-sm-12">
                                <div id="list_vendeurs" className="w3-display-container">
                                    <div id="tilte_vendeur_list">
                                        LISTE DES VENDEURS
                                    </div>
                                    <div className="loader w3-display-middle"></div>
                                    <div id="content_vendeur">
                                        <InfiniteScroll
                                            dataLength={this.state.vendeurs.length}
                                            next={null}
                                            hasMore={false}
                                            height={560}
                                        >
                                            {this.state.vendeurs.map((vendeur, key) => (
                                                <Vendeur key={key} pdp={vendeur.pdp} name={vendeur.nom} prenom={""} produit={vendeur.produit} adresse={vendeur.address} date={vendeur.date} />
                                            ))}

                                        </InfiniteScroll>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="newAndFavoriAnnonc">
                    <div className="container">
                        <div id="newContainer"><New /></div>
                        <p style={{ textAlign: "center", fontSize: "small",marginTop: 50 }}>We have created a fictional band website. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
                        <div id="favoriContainer">
                            <Favori />
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
export default Home;