import React, { useState, useEffect } from "react";
import Password from "../js/testPassword";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import "../css/inscription.css";
import { trim } from "jquery";
import { useDispatch } from "react-redux";
import { setTemp } from "../../../my_redux/actions/temp";
import { useAlert } from "react-alert";


const requireAsterix = <span style={{ color: "rgb(248, 133, 1)" }}>*</span>;

export default function Inscription() {

	const alert = useAlert();

	/////////////////////STATE////////////////////////
	const [username, setUsername] = useState({
		message: "",
		valid: false,
		value: ""
	});
	const [password, setPassword] = useState({
		message: "",
		valid: false,
		value: ""
	});
	const [email, setEmail] = useState({
		message: "",
		valid: false,
		value: ""
	});

	const [confirmationTest, setConfimationTest] = useState(false);

	const [termUrl, setTermUrl] = useState("#");

	const [warningPasswordMessage, setWarningPasswordMessage] = useState("");
	const [showPassword, setShowPassword] = useState({
		show: false,
		icon: "eye",
		type: "password"
	});
	const [TestPassword] = useState({
		instance: new Password()
	});

	const dispatch = useDispatch();

	/////////////////////////////////////////////////

	//////////////////PASSWORD/////////////////////////
	const passwordKeyUp = (password) => {
		let efficate = TestPassword.instance.passwordEfficacite(password);
		setPassword({
			message: efficate.message,
			valid: efficate.isValid,
			value: trim(password)
		});
		if (!efficate.isValid) {
			setWarningPasswordMessage(
				<p className="warningMessageFrom">Un mot de passe doit contenir au moins 8 caractères, un majuscule, un minuscule et un chiffre.</p>
			);
		} else {
			setWarningPasswordMessage("");
		}
	}

	const showPasswordClick = () => {
		if (showPassword.show) {
			setShowPassword({
				show: false,
				icon: "eye",
				type: "password"
			});
		} else {
			setShowPassword({
				show: true,
				icon: "eye-slash",
				type: "text"
			});
		}
	}
	/////////////////////////////////////////////////

	/////////////////USERNAME///////////////////////
	const usernameKeyUp = (username) => {
		fetch(process.env.REACT_APP_BACKEND_URL + "/user/username/notUnique/" + trim(username)).then(res => {
			res.json().then(notUnique => {
				if (notUnique.exist === true) {
					setUsername({
						message: "",
						valid: true,
						value: trim(username)
					});
				} else if (notUnique.exist === false) {
					setUsername({
						message: <span className="dangerMessageForm">Déjà utilisé</span>,
						valid: false
					});
				}
			}).catch(error => {
				alert.error("Error 404. Veuillez verifier votre accès à l'internet.");
			})
		}).catch(error => {
			alert.error("Error 404. Veuillez verifier votre accès à l'internet.");
		})
	}
	///////////////////////////////////////////////////////////////////////

	///////////////////////////////EMAIL/////////////////////////////////
	const emailKeyUp = (email) => {
		if (trim(email).match(/[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/)) {
			fetch(process.env.REACT_APP_BACKEND_URL + "/user/email/notUnique/" + trim(email)).then(res => {
				res.json().then(notUnique => {
					if (notUnique.exist === true) {
						setEmail({
							message: "",
							valid: true,
							value: trim(email)
						});
					} else if (notUnique.exist === false) {
						setEmail({
							message: <span className="dangerMessageForm">Déjà utilisé</span>,
							valid: false
						});
					}
				}).catch(() => {
					alert.error("Error 404. Veuillez verifier votre accès à l'internet.");
				})
			}).catch(() => {
				alert.error("Error 404. Veuillez verifier votre accès à l'internet.");
			})
		} else {
			setEmail({
				valid: false
			});
		}
	}
	//////////////////////////////////////////////////////////////////////

	///////////////////////Confirmation////////////////////////////////////
	const confirmationKeyUp = (confirmation) => {
		if (trim(confirmation) === trim(password.value) && password.valid) {
			setConfimationTest(true);
		} else {
			setConfimationTest(false);
		}
	}
	////////////////////////////////////////////////////////////////////

	/////////////////////SUBMIT_FORM///////////////////////////////////
	const formSubmit = (event) => {
		event.preventDefault();
		console.log(username.valid, password.valid, password.valid, confirmationTest);
		if (username.valid && email.valid && password.valid && confirmationTest) {
			var login = {
				username: username.value,
				password: password.value,
				email: email.value
			}
			dispatch(setTemp(login)); // add in temp information User
			window.location.assign("/front/registration/information_personnelle");
		} else if (!confirmationTest) {
			document.getElementById("confirmation").classList.add("confirmationError");
		}
	}
	/////////////////////////////////////////////////////////////////

	useEffect(() => {
		setTermUrl("#");
	}, []);

	return (
		<div className="container" >
			<div className="w3-display-container" style={{ width: "100%", height: 600 }}>
				<div className="registerContent w3-display-topmiddle">
					<div className="headerRegisterContent">
						<h1 className="my_title_1">CRÉER VOTRE COMPTE</h1>
					</div>
					<div className="bodyRegisterContent">
						<div className="errorContent" style={{ display: "none" }}></div>
						<form id="registerFrom" onSubmit={(event) => formSubmit(event)}>
							<label className="myLabel">Nom d'utilisateur{requireAsterix}</label>{username.message}<br />
							<input type="text" required id="username" onKeyUp={(event) => usernameKeyUp(event.currentTarget.value)} className="my-form-control" placeholder="Pseudo" />

							<label className="myLabel">Adresse mail{requireAsterix}</label>{email.message}<br />
							<input type="text" required id="email" onKeyUp={(event) => emailKeyUp(event.currentTarget.value)} className="my-form-control" placeholder="Email" />

							<div className="passwordContainer">
								<div className="btnShowPassword"><FontAwesomeIcon onClick={showPasswordClick} icon={showPassword.icon} /></div>
								<label className="myLabel" >Mot de passe{requireAsterix}</label>{password.message}<br />
								<input type={showPassword.type} required id="password" onKeyUp={(event) => passwordKeyUp(event.currentTarget.value)} className="my-form-control" placeholder="Mot de passe" />
								<div className="msgForm">{warningPasswordMessage}</div>
							</div>

							<div className="passwordContainer">
								<div className="btnShowPassword"><FontAwesomeIcon onClick={showPasswordClick} icon={showPassword.icon} /></div>
								<label className="myLabel">Confirmer le mot de passe{requireAsterix}</label><br />
								<input type={showPassword.type} required id="confirmation" onKeyUp={(event) => confirmationKeyUp(event.currentTarget.value)} className="my-form-control" placeholder="Confirmer le mot de passe" />
							</div>

							<label style={{
								fontWeight: "lighter",
								fontSize: "small",
								marginBottom: 20
							}}>
								<input type="checkbox" required id="terme" />&nbsp;&nbsp;J'accepte les termes et <a href={termUrl} style={{ color: "rgb(248, 133, 1)" }}>conditions générales</a>{requireAsterix}
							</label>
							<input type="submit" className="my-btn-form c-green" value="Suivant" />
						</form>
					</div>
				</div>
			</div>
		</div>
	);
}