import React, { useEffect } from "react";
import Axios from "axios";
import { useSelector, useDispatch } from "react-redux";
import { deleteTemp } from "../../../my_redux/actions/temp";


export default function RegistrationConfirmation(props) {

    const mail = useSelector(state => state.temp);
    const user = useSelector(state => state.logger.current_user);

    const dispatch = useDispatch();

    useEffect(() => {
        const params = props.match.params;
        Axios.get(process.env.REACT_APP_BACKEND_URL + "/user/checkConfirmation/" + params.token).then(res => {
            if (res.data.confirmation === true) {
                console.log(mail)
                Axios.patch(process.env.REACT_APP_BACKEND_URL + "/user/email/" + mail.id, mail, {
                    headers: {
                        "Authorization": "Barer " + user.token
                    }
                })
                    .then(res => {
                        dispatch(deleteTemp());
                        window.location.assign("/");
                    }).catch(err => {
                        dispatch(deleteTemp());
                        alert(err.message);
                    });
            } else {
                window.location.assign("/");
            }
        }).catch(err => {
            alert(err.message);
        })
    }, [dispatch, props.match.params, mail]);

    return (
        <></>
    )
}