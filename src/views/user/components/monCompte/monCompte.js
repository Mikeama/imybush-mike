import React, { useState, useEffect } from "react";
import "./css/monCompte.css";
import "./js/monCompteScript";
import { useSelector } from 'react-redux';
import { useAlert } from "react-alert";

import UserParameter from "./components/userParameter";
import ProduitManagerUser from "./components/produitManagerUser";
import Axios from "axios";

const url = process.env.REACT_APP_BACKEND_URL;

export default function MonCompte() {

    const user = useSelector(state => state.logger.current_user);
    const alert = useAlert();

    const [varTabContent, setTabContent] = useState(<ProduitManagerUser />);
    const [currentUser, setCurrentUser] = useState({});

    useEffect(() => {
        Axios.get(url + '/user/'+user.id)
            .then(res => { 
                setCurrentUser(res.data)
            })
            .catch(error => {
                alert.error(error.message);
            })
    }, [alert, user.id])

    return (
        <div className="container" style={{ marginBottom: 100, marginTop: 220 }}>
            <div className="tabContainer" style={{ width: "100%" }}>
                <button onClick={() => setTabContent(<ProduitManagerUser />)} className="tabBtn" id="firstTab">Liste de mes produits</button>
                <button onClick={() => setTabContent(<UserParameter currentUser={currentUser} />)} className="tabBtn">Paramètre de mon compte</button>
                <button onClick={() => setTabContent(<h1>tab3</h1>)} className="tabBtn">Information du site</button>
            </div>
            <div className="varTab">
                {varTabContent}
            </div>
        </div>
    );
}