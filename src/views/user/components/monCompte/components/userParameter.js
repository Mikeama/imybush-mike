import React, { useState, useEffect, useCallback } from "react";
import $ from "jquery";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCamera, faTimes } from '@fortawesome/free-solid-svg-icons'
import "../css/userParameter.css";
import Autocomplete from '@material-ui/lab/Autocomplete';
import ReactDropzone from "react-dropzone";
import { useAlert } from "react-alert";
import { setTemp } from "../../../../../my_redux/actions/temp";
import Axios from "axios";
import { trim } from "jquery";
import swal from "sweetalert";
import { useSelector, useDispatch } from 'react-redux';
import Password from "../../../js/testPassword";
import { loggoutUser } from "../../../../../my_redux/actions/logginUser";

const url = process.env.REACT_APP_BACKEND_URL;

const options = ['97139', '97121', '97122', '97123', '97100', '97125', '97130', '97140', '97113', '97127', '97126', '97112', '97190', '97128', '97129', '97111', '97160', '97170', '97131', '97110', '97116', '97117', '97133', '97120', '97118', '97134', '97150', '97180', '97115', '97136', '97137', '97114', '97141', '97119'];

const requireAsterix = <span style={{ color: "rgb(248, 133, 1)" }}>*</span>;

function previewFile(file, callback) {
    const reader = new FileReader();
    reader.onloadend = () => {
        callback(reader.result);
    };
    reader.readAsDataURL(file);
}

const styles = {
    width: 150,
    height: 150
};


export default function UserParameter(prop) {

    const dispatch = useDispatch();

    const alert = useAlert();
    const user = useSelector(state => state.logger.current_user);
    const [civilite, setCivilite] = useState({
        value: "",
        valide: false,
        messageError: ""
    });
    const [elementErrorPhotos, setElementErrorPhotos] = useState({
        valid: false,
        messageError: []
    });
    const [photos, setPhotos] = useState([]);

    const onDropRejectedFile = (arg) => {
        let errors = arg[0].errors;
        console.log(errors);
        let message = [];
        errors.forEach(error => {
            if (error.code === "file-invalid-type") {
                message.push(<span className="dangerMessageForm">Le fichier doit être une image.</span>);
            } else if (error.code === "too-many-files") {
                message.push(<span className="dangerMessageForm">Un fichier à la fois</span>);
            } else {
                message.push(<span className="dangerMessageForm">{error.message}</span>);
            }
        });
        setElementErrorPhotos({
            valid: false,
            messageError: message
        });
    }
    const onDropFile = (files) => {
        //responsiveImage()
        var tempFiles = [...photos];
        for (var i = 0; i < files.length; i++) {
            var photo = Object.assign(files[i], {
                preview: URL.createObjectURL(files[i])
            });
            tempFiles.push(photo);
        }
        setPhotos(tempFiles);
        setElementErrorPhotos({
            valid: true,
            messageError: []
        })
    }

    const deletePhoto = (key) => {
        let CopyPhotos = [...photos];
        let tempArray = [];
        for (var i = 0; i < CopyPhotos.length; i++) {
            if (i !== key) {
                tempArray.push(CopyPhotos[i]);
            }
        }
        console.log(tempArray);
        setPhotos(tempArray);
    }

    const [fileInfo, setFileInfo] = React.useState(null);
    const [id, setId] = useState(prop.currentUser._id);
    const [nom, setNom] = useState({
        value: "",
        valide: false,
        messageError: ""
    });
    const [prenom, setPrenom] = useState({
        value: "",
        valide: false,
        messageError: ""
    });
    const [username, setUsername] = useState({
        value: "",
        valide: false,
        messageError: ""
    });
    const [codePostal, setCodePostat] = useState({
        value: '',
        valide: false,
        messageError: ""
    });
    const [adresse, setAdresse] = useState({
        value: "",
        valide: false,
        messageError: ""
    });
    const [phone, setPhone] = useState({
        value: "",
        valide: false,
        messageError: ""
    });

    const [password, setPassword] = useState("");

    const [email, setEmail] = useState({
        messageError: "",
        valid: false,
        value: ""
    });

    const [confirmEmail, setConfirmEmail] = useState({
        messageError: "",
        valide: false
    })

    const [newPassword, setNewPassword] = useState({
        message: "",
        valid: false,
        value: ""
    })

    const [showPassword, setShowPassword] = useState({
        show: false,
        icon: "eye",
        type: "password"
    });

    const [warningPasswordSimilar, setWarningPasswordSimilar] = useState(null);

    const [warningPasswordMessage, setWarningPasswordMessage] = useState("");
    const [TestPassword] = useState({
        instance: new Password()
    });

    const [confirmMdp, setConfirmMdp] = useState({
        message: "",
        valide: false
    })

    ///////////////////////INITIALISATION DES DONNEES DU FORMULAIRE//////////////////////////////////////
    const initData = useCallback((currentUser) => {
        $('#civilite').val(currentUser.civilite);
        setCivilite({
            value: currentUser.civilite,
            valide: true,
            messageError: ""
        });
        $('#phone').val(currentUser.phone);
        setPhone({
            value: currentUser.phone,
            valide: true,
            messageError: ""
        });
        $('#nom').val(currentUser.nom);
        setNom({
            value: currentUser.nom,
            valide: true,
            messageError: ""
        });
        $('#prenom').val(currentUser.prenom);
        setPrenom({
            value: currentUser.prenom,
            valide: true,
            messageError: ""
        });
        $('#username').val(currentUser.username);
        setUsername({
            value: currentUser.username,
            valide: true,
            messageError: ""
        });
        $('#adresse').val(currentUser.adresse);
        setAdresse({
            value: currentUser.adresse,
            valide: true,
            messageError: ""
        });
        setPassword($('#currentPasswordInformation')[0].value)
    }, []);
    /////////////////////////////////////////////////////////////////////////////////////////

    const showPasswordClick = () => {
        if (showPassword.show) {
            setShowPassword({
                show: false,
                icon: "eye",
                type: "password"
            });
        } else {
            setShowPassword({
                show: true,
                icon: "eye-slash",
                type: "text"
            });
        }
    }

    ///////////////////////TELEPHONE/////////////////////////////////////////////////////
    const telKeyUp = (tel) => {
        const regexTel = /^\+?[0-9]+$/;
        if (regexTel.test(tel)) {
            setPhone({
                value: tel,
                valide: true,
                messageError: ""
            });
        } else {
            setPhone({
                value: "",
                valide: false,
                messageError: <span className="dangerMessageForm">Veuillez remplir correctement ce champ</span>
            });
        }
    }
    //////////////////////////////////////////////////////////////////////////////////////////

    //////////////////////////////////////////Enregistrement des modifications////////////////
    const saveChange = (event) => {
        event.preventDefault();
        console.log(nom.valide, prenom.valide, adresse.valide, username.valide, phone.valide, codePostal.valide, civilite.valide, password)
        testCodePostal(codePostal.value)
        if (nom.valide && prenom.valide && password !== "" && username.valide && adresse.valide && phone.valide && codePostal.valide && civilite.valide) {
            const formData = new FormData();
            console.log('true')
            formData.append("nom", nom.value);
            formData.append("prenom", prenom.value);
            formData.append("username", username.value);
            formData.append("adresse", adresse.value);
            formData.append("phone", phone.value);
            photos.forEach(image => {
                formData.append("images", image);
            });
            formData.append("codePostal", codePostal.value);
            formData.append("civilite", civilite.value);
            formData.append("current_password", password);
            Axios.patch(url + "/user/personal_information/" + id, formData, {
                headers: {
                    "Content-Type": "multipart/form-data",
                    "Authorization": "Barer " + user.token
                }
            })
                .then(res => {
                    swal({
                        title: "Modification réussie.",
                        icon: "success",
                        button: {
                            text: "Suivant",
                            value: true
                        }
                    }).then(() => {
                        window.location.assign("/front/mon_compte");
                    });
                })
                .catch(error => {
                    alert.error("Mots de passe incorrect!");
                })
        }
        else {
            console.log('false')
        }
    }
    /////////////////////////////////////////////////////////////////////////////////////////

    //////////////////PASSWORD/////////////////////////
    const passwordKeyUp = (password) => {
        let efficate = TestPassword.instance.passwordEfficacite(password);
        setNewPassword({
            message: efficate.message,
            valid: efficate.isValid,
            value: trim(password)
        });
        if (!efficate.isValid) {
            setWarningPasswordMessage(
                <p className="warningMessageFrom">Un mot de passe doit contenir au moins 8 caractères, un majuscule, un minuscule et un chiffre.</p>
            );
        } else {
            setWarningPasswordMessage("");
        }
    }

    const confirmationKeyUp = (mdp) => {
        if (mdp !== newPassword.value) {
            setConfirmMdp({
                message: <span className="dangerMessageForm">Mots de passe incorrect</span>,
                valide: false
            })
        }
        else {
            setConfirmMdp({
                message: "",
                valide: true
            })
        }
    }
    /////////////////////////////////////////////////////////////////////////////////////////

    const testCodePostal = (codePostal) => {
        if (!options.includes(codePostal)) {
            setCodePostat({
                value: "",
                valide: false,
                messageError: <span className="dangerMessageForm">Veuillez remplir correctement ce champ</span>
            })
        }
        else {
            setCodePostat({
                value: codePostal,
                valide: true,
                messageError: ""
            })
        }
    }

    const testsimilPass = (newP, mdp) => {
        if (newP === mdp) {
            setWarningPasswordSimilar(<span className="simple">Les mots de passes sont similaires</span>)
            setNewPassword({ ...newPassword, valid: false })
        }
        else {
            setWarningPasswordSimilar("")
            setNewPassword({ ...newPassword, valid: true })
        }
    }

    ///////////////////////////////SAVEPASSWORD///////////////////////////////
    const savePassword = (event) => {
        event.preventDefault();
        testsimilPass(newPassword.value, password)
        if (newPassword.valid && confirmMdp.valide && password !== "") {
            console.log(newPassword.value, confirmMdp.value, password)
            Axios.patch(url + "/user/password/" + id, {
                current_password: password,
                password: newPassword.value
            }, {
                headers: {
                    "Authorization": "Barer " + user.token
                }
            }).then(res => {
                dispatch(loggoutUser());
                window.location.assign("/front/login");
            })
        }
    }
    //////////////////////////////////////////////////////////////

    ///////////////EMAIL////////////////////////////////////////
    const emailKeyUp = (email) => {
        const regexEmail = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if (regexEmail.test(email)) {
            if (trim(email).match(/[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/)) {
                fetch(process.env.REACT_APP_BACKEND_URL + "/user/email/notUnique/" + trim(email)).then(res => {
                    res.json().then(notUnique => {
                        if (notUnique.exist === true) {
                            setEmail({
                                messageError: "",
                                valid: true,
                                value: trim(email)
                            });
                        } else if (notUnique.exist === false) {
                            setEmail({
                                messageError: <span className="dangerMessageForm">Déjà utilisé</span>,
                                valid: false
                            });
                        }
                    }).catch(() => {
                        alert.error("Error 404. Veuillez verifier votre accès à l'internet.");
                    })
                }).catch(() => {
                    alert.error("Error 404. Veuillez verifier votre accès à l'internet.");
                })
            } else {
                setEmail({
                    valid: false
                });
            }
        } else {
            setEmail({
                value: "",
                valid: false,
                messageError: <span className="dangerMessageForm">Ce n'est pas un adresse mail</span>
            });
        }
    }
    //////////////////////////////////////////////////////////////////////

    ///////////////////////////////////CONFIRMATION EMAIL///////////////////////////////////
    const confirmationEmail = (mail) => {
        if (email.value !== mail) {
            setConfirmEmail({
                messageError: <span className="dangerMessageForm">Les mails ne sont pas identique</span>,
                valide: false
            })
        }
        else {
            setConfirmEmail({
                messageError: "",
                valide: true
            })
        }
    }
    //////////////////////////////////////////////////////////////////////////////

    /////////////////////////////////SAVEEMAIL/////////////////////////////////////
    const saveEmail = (event) => {
        event.preventDefault();
        console.log(password)
        if (email.valid && confirmEmail.valide && password !== "") {
            var mail = {
                id: id,
                email: email.value,
                current_password: password,
            }
            console.log(mail)
            dispatch(setTemp(mail))
            Axios.get(url + "/user/email/exist/" + email.value + "?editEmail=true")
                .then(res => {
                    swal({
                        text: "Le lien de confirmation est envoyé dans votre adresse e-mail : " + email.value,
                        icon: "warning",
                        button: {
                            text: "Ok",
                            value: true
                        }
                    }).then(() => {
                        window.location.assign("/front");
                    });
                })
                .catch(error => alert.error(error))

        }
        else {
            console.log('clack')
        }
    }
    /////////////////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////DELETECOMPTE//////////////////////////////////////
    const deleteCompte = () => {
        swal({
            text: "Veuillez entrer votre mots de passe:",
            content: {
                element: "input",
                attributes: {
                    placeholder: "Votre mots de passe",
                    type: "password",
                    id: "validePassword"
                }
            },
            dangerMode: true,
        }).then((mdp) => {

            Axios.post(url + "/user/delete/" + id, {
                current_password: mdp
            }, {
                headers: {
                    "Authorization": "Barer " + user.token
                }
            })
                .then(res => {
                    swal("Suppression terminer", "", "success").then(() => {
                        dispatch(loggoutUser())
                        window.location.assign("/front")
                    })
                })
        })
    }
    //////////////////////////////////////////////////////////////////////

    useEffect(() => {
        initData(prop.currentUser);
        $(document).ready(() => {
            $("#deleteBtnContainer").width($("#paramterAccountContainer").width());
            $(window).resize(() => {
                $("#deleteBtnContainer").width($("#paramterAccountContainer").width());
            });
        });
    }, [alert, initData, prop.currentUser]);

    console.log(prop.currentUser)

    return (
        <>
            <div id="paramterAccountContainer" style={{ marginTop: 20, width: "100%" }}>
                <div className="parameterCompteContainer">
                    <div className="headerContainerParameter">
                        <h1 className="titleHeaderParameter">Mes informations personnelles</h1>
                    </div>
                    <div className="bodyContainerParameter">
                        <div className="w3-row">
                            <div className="picContainer">
                                {photos.map((photo, key) => (
                                    <div key={key} className="w3-col s3 imgContainer" style={{zIndex:"100", borderRadius:"100%"}}>
                                        <div className="imageContent bord-dash bord-dash-orange" style={{ borderRadius:"100%" }}>
                                            <img src={photo.preview} style={{ width: "100%",borderRadius:"100%" }} className="imgDrop" alt={"image_" + key} />
                                        </div>
                                        <div className="deletePhotoContainer w3-display-container" style={{ width: "100%", height: "100%" }}>
                                            <div className="deletePhoto w3-display-bottomright" onClick={() => deletePhoto(key)}><FontAwesomeIcon style={{ marginTop: 2 }} icon={faTimes} /></div>
                                        </div>
                                    </div>
                                ))}
                                <ReactDropzone
                                    multiple={false}
                                    onDropRejected={(arg) => onDropRejectedFile(arg)}
                                    onDrop={(files) => onDropFile(files)}
                                    accept="image/*">
                                    {({ getRootProps, getInputProps }) => (
                                        <div {...getRootProps()}>
                                            <input {...getInputProps()} />
                                            <div style={{borderRadius:"100%"}} className="w3-display-container imageContent bord-dash bord-dash-gray">
                                                <img src={url + "/images/" + prop.currentUser.photo} className="bgphoto" alt="logo" />
                                                <div className="w3-display-middle">
                                                    <span style={{
                                                        fontSize: 30,
                                                        opacity: 0.8,
                                                        color:"white"
                                                    }}><FontAwesomeIcon icon={faCamera} /></span>
                                                </div>
                                            </div>
                                        </div>
                                    )}
                                </ReactDropzone>
                            </div>
                        </div>
                        <form id="formInfoPeronnelle" onSubmit={(event) => saveChange(event)}>
                            <div className="w3-row">
                                <div className="w3-col responsive" style={{ width: "47%" }}>
                                    <label className="myLabel">Civilité{requireAsterix}</label><br />
                                    <select className="my-form-control" onChange={(event) => setCivilite(event.currentTarget.value)} id="civilite">
                                        <option value={"M."}>M. (monsieur)</option>
                                        <option value={"Mme."}>Mme (madame)</option>
                                        <option value={"Mlle."}>Mlle. (mademoiselle)</option>
                                    </select>
                                    <label className="myLabel">Nom{requireAsterix}</label><br />
                                    <input type="text" required className="my-form-control" onChange={(event) => setNom({ ...nom, value: event.currentTarget.value })} id="nom" />

                                    <label className="myLabel">Prenom{requireAsterix}</label>{prenom.messageError}<br />
                                    <input type="text" required className="my-form-control" onChange={(event) => setPrenom({ ...prenom, value: event.currentTarget.value })} id="prenom" />

                                    <label className="myLabel">Pseudo{requireAsterix}</label>{username.messageError}<br />
                                    <input type="text" required className="my-form-control" onChange={(event) => setUsername({ ...username, value: event.currentTarget.value })} id="username" />
                                </div>
                                <div className="w3-col responsive" style={{ width: "6%", opacity: 0 }}>crack</div>
                                <div className="w3-col responsive" style={{ width: "47%" }}>
                                    <label className="myLabel">Adresse{requireAsterix}</label>{adresse.messageError}<br />
                                    <input type="text" required className="my-form-control" onChange={(event) => setAdresse({ ...adresse, value: event.currentTarget.value, valide: true })} id="adresse" />

                                    <Autocomplete
                                        id="custom-input-demo"
                                        options={options}
                                        onInputChange={() => {
                                            $(function () {
                                                setCodePostat({
                                                    value: $('#codePostale').val(),
                                                    valide: true,
                                                    messageError: ""
                                                })
                                            })
                                        }
                                        }
                                        renderInput={(params) => (
                                            <div ref={params.InputProps.ref}>
                                                <label className="myLabel">Code postal{requireAsterix}</label>{codePostal.messageError}<br />
                                                <input style={{
                                                    width: "100%",
                                                    height: "35px",
                                                    borderRadius: 4,
                                                    border: "1px solid rgba(0,0,0, 0.3)",
                                                }} className="my-form-control" type="text" {...params.inputProps} id="codePostale" />
                                            </div>
                                        )}
                                    />

                                    <label style={{ marginTop: 20 }} className="myLabel">Téléphone{requireAsterix}</label>{phone.messageError}<br />
                                    <input type="text" required className="my-form-control" onKeyUp={(event) => telKeyUp(event.currentTarget.value)} id="phone" />

                                    <label className="myLabel">Veuillez entrez votre mot de passe{requireAsterix}</label><br />
                                    <input type="password" required className="my-form-control" id="currentPasswordInformation" onKeyUp={(event) => setPassword(event.currentTarget.value)} />
                                </div>
                                <div className="w3-row">
                                    <div className="w3-col" style={{ width: "100%", marginTop: 20, marginBottom: 20 }}>
                                        <div className="w3-display-container">
                                            <div className="w3-display-right">
                                                <button type="submit" className="btn-paramter c-green">Modifier mon information</button>&nbsp;
                                            <button type="button" className="btn-paramter annulerBtnParameter">Annuler</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div className="parameterCompteContainer">
                    <div className="headerContainerParameter">
                        <h1 className="titleHeaderParameter">Mon Email</h1>
                    </div>
                    <div className="bodyContainerParameter">
                        <form id="emailParameterForm" onSubmit={(event) => saveEmail(event)}>
                            <label className="myLabel">Entrez votre nouveau Adresse Mail{requireAsterix}</label>{email.messageError}<br />
                            <input type="email" className="my-form-control" onKeyUp={(event) => emailKeyUp(event.currentTarget.value)} required id="newMail" />

                            <label className="myLabel">Confirmez votre email{requireAsterix}</label>{confirmEmail.messageError}<br />
                            <input type="email" className="my-form-control" onKeyUp={(event) => confirmationEmail(event.currentTarget.value)} required id="newEmailchangeEmail" />

                            <label className="myLabel">Veuillez entrez votre mot de passe{requireAsterix}</label><br />
                            <input type="password" required className="my-form-control" onKeyUp={(event) => setPassword(event.currentTarget.value)} id="curentPasswordMail" />

                            <div style={{ width: "100%", marginTop: 20, marginBottom: 40 }}>
                                <div className="w3-display-container">
                                    <div className="w3-display-right">
                                        <button type="submit" className="btn-paramter c-green">Modifier mon information</button>&nbsp;
                                    <button type="button" className="btn-paramter annulerBtnParameter">Annuler</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div className="parameterCompteContainer">
                    <div className="headerContainerParameter">
                        <h1 className="titleHeaderParameter">Mon mot de passe</h1>
                    </div>
                    <div className="bodyContainerParameter">
                        <form id="passwordParameterForm" onSubmit={(event) => savePassword(event)}>
                            <div className="passwordContainer">
                                <div className="btnShowPassword"><FontAwesomeIcon onClick={showPasswordClick} icon={showPassword.icon} /></div>
                                <label className="myLabel">Entrez votre nouveau mot de passe{requireAsterix}</label>{newPassword.message}<br />
                                <input type={showPassword.type} className="my-form-control" onKeyUp={(event) => passwordKeyUp(event.currentTarget.value)} required id="newPassword" />
                                <div className="msgForm">{warningPasswordMessage}</div>
                            </div>

                            <div className="passwordContainer">
                                <div className="btnShowPassword"><FontAwesomeIcon onClick={showPasswordClick} icon={showPassword.icon} /></div>
                                <label className="myLabel">Confirmez votre nouveau mot de passe{requireAsterix}</label>{confirmMdp.message}<br />
                                <input type={showPassword.type} onKeyUp={(event) => confirmationKeyUp(event.currentTarget.value)} className="my-form-control" required id="confirmationPassword" />
                            </div>
                            <label className="myLabel">Veuillez entrez votre mot de passe actuel{requireAsterix}</label>{warningPasswordSimilar}<br />
                            <input type="password" required onKeyUp={(event) => setPassword(event.currentTarget.value)} className="my-form-control" id="curentPasswordPwd" />

                            <div style={{ width: "100%", marginTop: 20, marginBottom: 40 }}>
                                <div className="w3-display-container">
                                    <div className="w3-display-right">
                                        <button type="submit" className="btn-paramter c-green">Modifier mon information</button>&nbsp;
                                    <button type="button" className="btn-paramter annulerBtnParameter">Annuler</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div id="deleteBtnContainer" className="test" style={{ marginTop: 70, marginBottom: 40, position: "absolute" }}>
                    <div className="w3-display-container">
                        <div className="w3-display-right" style={{ marginRight: 50 }}>
                            <button type="button" className="deleteAccounParameter" onClick={deleteCompte}>suprimer mon compte</button>
                        </div>
                        <div className="w3-display-left" style={{ fontSize: "small", marginTop: -30 }}>
                            {requireAsterix}:&nbsp;champs obligatoires
                    </div>
                    </div>
                </div>
            </div>
        </>
    );
}