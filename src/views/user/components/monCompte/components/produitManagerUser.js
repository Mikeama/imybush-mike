import React, { useState, useEffect } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import ListProduit from "./listProduit";
import Pagination from "../../../../../components/pagination/pagination";
import "../css/produitManagerUser.css";
import responsiveInit from "../js/produitManagerUserScript";
import { useSelector } from "react-redux";
import Axios from "axios";
import swal from 'sweetalert';
import $ from 'jquery';

const url = process.env.REACT_APP_BACKEND_URL;

export default function ProduitManagerUser(prop) {

    const user = useSelector(state => state.logger.current_user);
    const [pagination, setPagination] = useState(null);
    const [allProduits, setAllProduits] = useState([]);
    const [online, setOnline] = useState(0);
    const [annonceEnLigne, setAnnonceEnLigne] = useState(true);
    const [annonceExpire, setAnnonceExpire] = useState(false);
    const [expired, setExpired] = useState(0);
    const [dispoStatus, setDispoStatus] = useState("online");
    const [page, setPage] = useState(1);
    const [sort, setSort] = useState("-createdAt");
    const [listCheck, setListCheck] = useState([]);

    useEffect(() => {
        getUserAnnonce(page, sort);
        responsiveInit();
    }, []);

    //////////////Séléction et ajout d'un élèment à supprimer////////////////
    const oneCheck = (e) => {
        if (e.currentTarget.checked === true) {
            listCheck.push(e.currentTarget.value);
            setListCheck([...listCheck]);
        }
        else {
            let index = listCheck.indexOf(e.currentTarget.value);
            listCheck.splice(index, 1);
            setListCheck([...listCheck]);
        }
    }
    ////////////////////////////////////////////////////////////////////////

    
    ///////////////////////////////////Selectionner tout/////////////////////////////////////
    const allCheck = (e) => {
        if (e.currentTarget.checked === true) {
            $(function () {
                let varb = $('.check').prop('checked', true);
                for (let i = 1; i < varb.length; i++)
                    listCheck.push(varb[i].defaultValue);
                setListCheck([...listCheck]);
            })
        }
        else {
            $(function () {
                $('.check').prop('checked', false);
                setListCheck([]);
            })
        }
    }
    ////////////////////////////////////////////////////////////////////////
    const getUserAnnonce = (page, sort, dispoStatus = "online") => {
        Axios.get(url + '/annonce?page=' + page + '&user=' + user.id + "&sort=" + sort + "&dispoStatus=" + dispoStatus)
            .then(res => {
                setOnline(res.data.countOnline);
                setExpired(res.data.countExpired);
                if (res.data.results.length >= 1 ) {
                    const pagination = <Pagination count={Math.ceil(res.data.count / 10)} onChange={onChangePage} />;
                    setPagination(pagination);
                }
                else
                    setPagination(null);
                const temp = [];
                res.data.results.map(produit => {
                    const prod = {
                        id: produit._id,
                        name: produit.title,
                        prix: produit.price,
                        photo: produit.images[0],
                        status: {
                            value: produit.etatStock,
                            soliciteur: 0
                        },
                        localisation: {
                            lat: produit.localisation.lat,
                            lng: produit.localisation.lng
                        },
                        dateDispo: {
                            debut: produit.disponibility.debut,
                            fin: produit.disponibility.fin
                        },
                        dateAjout: produit.createdAt,
                        tel: produit.telephone,
                        email: produit.email
                    };
                    temp.push(<ListProduit key={prod.id} data={prod} onCheck={oneCheck} />)
                })
                setAllProduits(temp);
            })
    }

    const onChangePage = (page) => {
        getUserAnnonce(page,sort,dispoStatus);
    }

    const redirectProduit = () => {
        window.location.assign("/front/proposer_produit");
    }

    const DelChecked = (listCheck) => {
        Axios.post(url + '/annonce/delete_many', {
            annonces: listCheck
        }, {
            headers: {
                "Authorization": "Barer " + user.token
            }
        }).then(res => {
            window.location.assign("/front/mon_compte");
        })
    }

    /////////////////////////////////Vérification des checkbox avant de les supprimer///////////////////////////////////////
    const testSupp = (listCheck) => {
        if (listCheck.length !== 0) {

            swal({
                title: "Confirmation",
                text: "Voulez-vous vraiment supprimer les produits?",
                icon: "warning",
                buttons: {
                    cancel: "Annuler",
                    confirm: "Confirmer"
                },
                dangerMode: true,
            })
                .then((willDelete) => {
                    if (willDelete) {
                        DelChecked(listCheck)
                        swal("Produits supprimés", {
                            icon: "success",
                        });
                    }
                })
        }
    }
    ////////////////////////////////////////////////////////////////////////

    ///////////////////////////////Tri Select/////////////////////////////////////////
    const changeSelect = (e) => {
        if (e.currentTarget.value === "Plus Anciennes") {
            getUserAnnonce(1, "createdAt", dispoStatus);
        }
        else {
            getUserAnnonce(1, "-createdAt", dispoStatus);
        }
    }
    ////////////////////////////////////////////////////////////////////////

    return (
        <div style={{ width: "100%", marginTop: 40 }}>
            <div className="w3-row">
                <div className="w3-col responsive" style={{ width: "75%", marginBottom: 20 }}>
                    <button className={annonceEnLigne?"btnDateStatus c-green":"btnDateStatus expiredBtn"} onClick={() => {
                        setAnnonceEnLigne(true)
                        getUserAnnonce(page, sort)
                        setDispoStatus("online")
                        setListCheck([])
                        setAnnonceExpire(false)
                    }}>Annonces en ligne ({online})</button>&nbsp;&nbsp;
                    <button className={annonceExpire?"btnDateStatus c-green":"btnDateStatus expiredBtn"} onClick={() => {
                        setAnnonceExpire(true)
                        getUserAnnonce(page, sort, "expired")
                        setDispoStatus("expired")
                        setListCheck([])
                        setAnnonceEnLigne(false)
                    }}>Annonces expirées ({expired})</button>
                </div>
                <div className="w3-col responsive" style={{ width: "25%" }}>
                    <div className="sortInputProduitManager w3-row">
                        <div className="w3-col" style={{ width: "40px" }}>
                            <label className="labelSortProduitManager">Tri:</label>
                        </div>
                        <div className="w3-rest">
                            <select className="selectInputProduitManager" onChange={changeSelect}>
                                <option defaultValue="lastest">
                                    Plus récentes
                                </option>
                                <option defaultValue="lastest">
                                    Plus Anciennes
                                </option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div className="w3-display-container" style={{ marginTop: 40 }}>
                <div className="w3-display-right">
                    <div className="btnProduitManagerContainer" style={{ width: "100%" }}>
                        <button className="btnProduitManager" onClick={() => redirectProduit()}><FontAwesomeIcon icon="plus-circle" />&nbsp;Ajouter un produit</button>&nbsp;&nbsp;
                        <button className="btnProduitManager" onClick={() => testSupp(listCheck)}><FontAwesomeIcon icon="trash" />&nbsp;Suprimer</button>
                    </div>
                </div>
                <div className="w3-display-left" style={{ fontSize: "small" }}>
                    <div style={{ minWidth: 150 }}><input type="checkbox" id="selectAllInputProduitManager" className="check" onClick={allCheck} /> Tout séléctionner <br /></div>
                </div>
            </div>
            <div className="underlineProduitManager"></div>
            <div id="test"></div>
            <div className="listCOntainerProduitManager">
                {
                    allProduits
                }
            </div>
            <div className="paginationContainer">
                <div className="w3-display-container">
                    <div className="w3-display-middle">
                        {
                            pagination
                        }
                    </div>
                </div>
            </div>
        </div>
    );
}