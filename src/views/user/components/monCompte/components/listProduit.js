import React, { useEffect, useState } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import responiveListProduit from "../js/listProduitScript";
import "../css/listproduit.css";
import Axios from 'axios';
import { useSelector } from 'react-redux';
import swal from 'sweetalert';
import fonction from '../../../../../components/function/function';

const url = process.env.REACT_APP_BACKEND_URL;

export default function ListProduit(prop) {

    const myFonction = new fonction();

    const user = useSelector(state => state.logger.current_user);

    const [nameProduit, setNameProduit] = useState("");
    const [prix, setPrix] = useState("");
    const [status, setStatus] = useState({
        text: "",
        value: "",
        soliciteur: 0
    });
    const [dateDispo, setDateDispo] = useState({
        debut: "",
        fin: ""
    })
    const [dateAjout, setDateAjout] = useState("");
    const [adresse, setAdresse] = useState("");
    const [tel, setTel] = useState("");
    const [email, setEmail] = useState("");
    const [id, setId] = useState();



    useEffect(() => {
        setNameProduit(prop.data.name);
        setPrix(prop.data.prix);
        setStatus({
            text: prop.data.status.value,
            value: prop.data.status.value === "En stock" ? "successListProduit" : (prop.data.status.value === "Hors stock" ? "dangerListProduit" : "none"),
            soliciteur: prop.data.status.soliciteur ? prop.data.status.soliciteur : 0
        });
        setId(prop.data.id)
        setDateAjout(myFonction.changeDate(new Date(prop.data.dateAjout).toLocaleDateString()));
        setDateDispo({
            debut: myFonction.changeDate(new Date(prop.data.dateDispo.debut).toLocaleDateString()),
            fin: myFonction.changeDate(new Date(prop.data.dateDispo.fin).toLocaleDateString())
        });
        setTel(prop.data.tel);
        setEmail(prop.data.email);
        responiveListProduit();
        Axios.get("https://api-adresse.data.gouv.fr/reverse/?lon=" + prop.data.localisation.lng + "&lat=" + prop.data.localisation.lat)
            .then(res => setAdresse(res.data.features[0].properties.name))
            .catch(error => alert.error(error));
    }, [prop.data.name, prop.data.prix, prop.data.status, prop.data.dateAjout, prop.data.localisation.lng, prop.data.localisation.lat, prop.data.dateDispo.debut, prop.data.dateDispo.fin, prop.data.adresse, prop.data.tel, prop.data.email, prop.data.id]);

    const supprimer = (id) => {
        Axios.delete(url + "/annonce/" + id, {
            headers: {
                "Authorization": "Barer " + user.token,
            }
        })
            .then(res => {
                console.log("Item has deleted");
                window.location.assign("/front/mon_compte");
            })
    }

    return (
        <div className="listProduitManager">
            <div className="w3-row rowContainerProduitManager" style={{ minHeight: 140 }}>
                <div className="w3-col checkboxContainer" style={{ width: 23 }}>
                    <input style={{ marginTop: 62 }} type="checkbox" onClick={prop.onCheck} className="check" value={id} />
                </div>
                <div className="w3-col imageContainerListProduit" style={{ width: 140, marginRight: 20 }}>
                    <img style={{ height: 140, width: 140, borderRadius: 4, objectFit: "cover" }} src={url + "/images/" + prop.data.photo} alt={prop.data.name} />
                </div>
                <div className="w3-col InformationContainer" style={{ overflow: "visible" }}>
                    <div className="w3-row">
                        <div className="w3-col borderRight" style={{ width: "81.5%" }}>
                            <div className="w3-display-container informationContainer" style={{ minHeight: 140 }}>
                                <div className="w3-display-left minInformationContainer" style={{ marginTop: -10 }}>
                                    <div className="nameProduitContentListProduit">{nameProduit}</div>
                                    <div className="prixContentListProduit">{prix}€</div>
                                    <div className={status.value} style={{ minHeight: 20 }}>{status.text}</div>
                                </div>
                                <div className="w3-display-middle buttonViewListProduitContainer1" style={{ marginTop: 5 }}>
                                    <div className="popoverClick1 buttonViewListProduit1" style={{ paddingTop: 7 }}>Voir informations</div>
                                </div>
                                <div className="w3-display-middle buttonViewListProduitContainer" style={{ zIndex: 1 }}>
                                    <div className="popperContainer buttonViewListProduit" >
                                        <div className="popoverClick">Voir informations</div>&nbsp;
                                        <div className="popperRight hideInformation informationContent">
                                            <div className="caret-left mycaret"></div>
                                            <div className="closeInformation"><FontAwesomeIcon icon="times" /></div>
                                            <div className="headerInformationContent">
                                                Informations
                                            </div>
                                            <div className="bodyInformationContent">
                                                <div className="w3-row">
                                                    <div className="w3-col" style={{ width: "40%" }}>
                                                        <div className="nameProduitInInformationContent">{nameProduit}</div>
                                                        <div className="prixInformationContent">{prix}€</div>
                                                    </div>
                                                    <div className="w3-col" style={{ width: "60%", marginTop: 5 }}>
                                                        <div className={"floatRight " + (status.soliciteur > 0 ? "soliciteurInformation" : "")}>{status.soliciteur < 1 ? "" : ("Soliciteur: " + status.soliciteur)}</div>
                                                        <div className={"floatRight " + (status.soliciteur < 1 ? "successStatusInformation" : "dangerStatusInformation")}>{status.text}</div>
                                                    </div>
                                                </div>
                                                <div className="dispoContainerInformationList">
                                                    <label className="myLabel">Période de disponibilité</label>
                                                    <div className="w3-display-container" style={{ minHeight: 70 }}>
                                                        <div className="w3-display-middle">
                                                            <div className="dateDispoInformationListProduit">{dateDispo.debut}</div>
                                                            <div style={{ marginTop: 3, marginBottom: 3, textAlign: "center" }}>Jusqu'au</div>
                                                            <div className="dateDispoInformationListProduit">{dateDispo.fin}</div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="userInformationContainer" style={{ marginTop: 20 }}>
                                                    <div className="userinformation"><FontAwesomeIcon icon="home" />&nbsp;{adresse}</div>
                                                    <div className="userinformation"><FontAwesomeIcon icon="phone" />&nbsp;{tel}</div>
                                                    <div className="userinformation"><FontAwesomeIcon icon="envelope" />&nbsp;{email}</div>
                                                </div>
                                            </div>
                                        </div>
                                        {/* <span className="popperBottom"><div className="caret-top"></div>Tooltip text1</span> */}
                                    </div>
                                </div>
                                <div className="w3-display-bottomright">
                                    <div className="dateAjoutListProduit">{dateAjout}</div>
                                </div>
                            </div>
                        </div>
                        <div className="w3-col" style={{ width: "18.5%" }}>
                            <div className="w3-display-container" style={{ minHeight: 140 }}>
                                <div className="w3-display-middle">
                                    <span onClick={() => window.location.assign("/front/modifier_produit/" + id)} style={{ color: "rgba(0, 187, 0, 0.87)" }}><FontAwesomeIcon icon="edit" /></span>&nbsp;&nbsp;&nbsp;&nbsp;
                                    <span style={{ color: "rgb(255, 85, 85)" }}><FontAwesomeIcon icon="trash"
                                        onClick={() =>
                                            swal({
                                                title: "Confirmation",
                                                text: "Voulez-vous vraiment supprimer ce produit?",
                                                icon: "warning",
                                                buttons: {
                                                    cancel: "Annuler",
                                                    confirm: "Confirmer"
                                                },
                                                dangerMode: true,
                                            })
                                                .then((willDelete) => {
                                                    if (willDelete) {
                                                        supprimer(id)
                                                        swal("Produit supprimé", {
                                                            icon: "success",
                                                        });
                                                    }
                                                })
                                        }
                                    /></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
} 