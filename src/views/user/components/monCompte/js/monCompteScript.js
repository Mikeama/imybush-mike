import $ from "jquery";

$(document).ready(() => {
    buildTab(document.getElementById("firstTab"));

    $(".tabBtn").click((e) => {
        buildTab(e.currentTarget);
    });

    function buildTab(element) {
        $(".tabBtn").css({
            "border-radius": "4px 4px 0px 0px",
            "height": "50px",
            "font- size": "small",
            "font-weight": "bold",
            "padding-left": "2%",
            "padding-right": "2%",
            "border": "none",
            "background-color": "transparent",
            "color": "black"
        });
        $(element).css({
            "border-left": "1px solid rgba(0, 0, 0, 0.274)",
            "border-right": "1px solid rgba(0, 0, 0, 0.274)",
            "border-top": "1px solid rgba(0, 0, 0, 0.274)",
            "border-bottom": "1px solid white",
            "color": "orange"
        });
        if(element) {
            if (element.getAttribute('id') === "firstTab") {
                $(".varTab").css({
                    "border-radius": "0 4px 4px 4px",
                    "margin-top": "-1px",
                    "border": "1px solid rgba(0, 0, 0, 0.274)",
                    "width": "100%",
                    "padding": "1.5%"
                });
            } else {
                $(".varTab").css({
                    "border-radius": "4px 4px 4px 4px",
                    "margin-top": "-1px",
                    "border": "1px solid rgba(0, 0, 0, 0.274)",
                    "width": "100%",
                    "padding": "1.5%"
                });
            }
        }
    }
});