import $ from "jquery";

const responiveListProduit = () => {
    responsive();
    popper();
    $(window).resize(() => {
        popper();
        responsive();
    });

    var elementinformationBtn = document.getElementsByClassName("popoverClick");
    for (var i = 0; i < elementinformationBtn.length; i++) {
        elementinformationBtn[i].addEventListener("click", (event) => {
            var element = event.currentTarget.parentNode.parentNode.children[0].children[1];
            var path = event.path || (event.composedPath && event.composedPath()); 
            closeAll(path[1]);
            element.classList.remove("hideInformation");
            $(".buttonViewListProduit").css({
                "background": "rgba(0, 187, 0, 0.87)",
                "color": "white"
            })
        });
    }

    $(".closeInformation").click(() => {
        closeAll();
        $(".buttonViewListProduit").css({
            "background": "white",
            "color": "rgba(0, 187, 0, 0.87)"
        })
    });

    $(".popoverClick1").click((event) => {
        closeAll();
        var element = event.currentTarget.parentNode.nextSibling.children[0].lastChild;
        closeAll(element.parentNode);
        element.classList.remove("hideInformation");
    });
}

function closeAll(element) {
    var informationContent = document.getElementsByClassName('informationContent');
    var buttonViewListProduit = document.getElementsByClassName('buttonViewListProduit');
    for (var k = 0; k < informationContent.length; k++) {
        informationContent[k].classList.add("hideInformation");
        if (element) {
            if (buttonViewListProduit[k] !== element) {
                buttonViewListProduit[k].style.display = "none";
            }
        } else {
            buttonViewListProduit[k].style.display = "block";
        }
    }
}

function responsive() {
    var rowContainerProduitManager = $(".rowContainerProduitManager").width();
    var widthcheckboxContainer = $(".checkboxContainer").width();
    var imageContainerListProduit = $(".imageContainerListProduit").width();
    var widthInformationContainer = (rowContainerProduitManager - (widthcheckboxContainer + imageContainerListProduit));
    $(".InformationContainer").width(widthInformationContainer - 20);
    var elementBtnShowInformation = document.getElementsByClassName("buttonViewListProduitContainer");
    var elementBtnShowInformation1 = document.getElementsByClassName("buttonViewListProduitContainer1");
    var minInformationContainer = document.getElementsByClassName("minInformationContainer");
    var windowWidth = $(window).width();
    if (windowWidth >= 760) {
        for (var k = 0; k < elementBtnShowInformation.length; k++) {
            minInformationContainer[k].classList.add("w3-display-left");
            minInformationContainer[k].classList.remove("w3-display-topleft");
            elementBtnShowInformation[k].classList.remove("w3-display-left");
            elementBtnShowInformation[k].classList.add("w3-display-middle");
            elementBtnShowInformation1[k].classList.remove("w3-display-left");
            elementBtnShowInformation1[k].classList.add("w3-display-middle");
        }
        $(".minInformationContainer").css("margin-top", "-10px");
        $(".buttonViewListProduitContainer").css("margin-top", "0px");
        $(".buttonViewListProduitContainer1").css("margin-top", "0px");

    } else if (windowWidth < 760) {
        for (var i = 0; i < elementBtnShowInformation.length; i++) {
            minInformationContainer[i].classList.remove("w3-display-left");
            minInformationContainer[i].classList.add("w3-display-topleft");
            elementBtnShowInformation[i].classList.remove("w3-display-middle");
            elementBtnShowInformation[i].classList.add("w3-display-left");
            elementBtnShowInformation1[i].classList.remove("w3-display-middle");
            elementBtnShowInformation1[i].classList.add("w3-display-left");
        }
        $(".minInformationContainer").css("margin-top", "0px");
        $(".buttonViewListProduitContainer").css("margin-top", "27px");
        $(".buttonViewListProduitContainer1").css("margin-top", "27px");
    }

    var informationContainer = $(".informationContainer");
    var minInformation = $(".minInformationContainer");
    for (var m = 0; m < minInformation.length; m++) {
        if (minInformation[m].clientHeight > 72) {
            informationContainer[m].style.height = (minInformation[m].clientHeight * 2) + "px";
        }
        else {
            informationContainer[m].style.height = "140px";
            informationContainer[m].style.position = "normal";
        }
    }
}

function popper() {
    var windowWidth = $(window).width();
    if (windowWidth < 984) {
        for (var i = 0; i < document.getElementsByClassName('informationContent').length; i++) {
            document.getElementsByClassName('informationContent')[i].classList.remove("popperRight");
            document.getElementsByClassName('informationContent')[i].classList.add("popperBottom");
            document.getElementsByClassName('mycaret')[i].classList.remove("caret-left");
            document.getElementsByClassName('mycaret')[i].classList.add("caret-top");
        }
    } else {
        for (var k = 0; k < document.getElementsByClassName('informationContent').length; k++) {
            document.getElementsByClassName('informationContent')[k].classList.remove("popperBottom");
            document.getElementsByClassName('informationContent')[k].classList.add("popperRight");
            document.getElementsByClassName('mycaret')[k].classList.remove("caret-top");
            document.getElementsByClassName('mycaret')[k].classList.add("caret-left");
        }
    }
}

export default responiveListProduit;