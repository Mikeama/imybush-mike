
import $ from "jquery";

const responsiveInit = () => {
    responsive();
    $(window).resize(() => {
        responsive();
    });
}
function responsive() {
    var windowWidth = $(window).width();
    $(".paginationContainer").width($(".listCOntainerProduitManager").width());
    if (windowWidth < 564) {
        $(".underlineProduitManager").css("margin-top", "120px")
        $(".btnProduitManagerContainer").css("margin-top", "80px");
    } else {
        $(".btnProduitManagerContainer").css("margin-top", "0");
        $(".underlineProduitManager").css("margin-top", "90px");
    }
}
export default responsiveInit;