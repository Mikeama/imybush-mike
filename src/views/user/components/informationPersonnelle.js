import React, { useState } from "react";
import "../css/informationPersonnelle.css";
import { useSelector, useDispatch } from "react-redux";
import { setTemp } from "../../../my_redux/actions/temp";
import iconSecure from "../../../assets/icon/icon_secure.png";
import $ from "jquery";
import Axios from "axios";

const requireAsterix = <span style={{ color: "rgb(248, 133, 1)" }}>*</span>;

export default function InformationPersonnelle() {

    const [civilite, setCivilite] = useState("");
    const [nom, setNom] = useState("");
    const [prenom, setPrenom] = useState("");
    const [adresse, setAdresse] = useState("");
    const [codePostal, setCodePostal] = useState("");
    const [phone, setPhone] = useState("");

    const user = useSelector(state => state.temp);

    const dispatch = useDispatch();

    const formSubmit = (e) => {
        e.preventDefault();
        if (civilite !== null) {
            dispatch(setTemp({ ...user, civilite, nom, prenom, adresse, codePostal, phone }));
            $("#formInformation").fadeOut(500, () => {
                $("#confirmationMsg").fadeIn();
            });
            Axios.get(process.env.REACT_APP_BACKEND_URL + "/user/email/exist/" + user.email+"?registration=true").then(res => {
                console.log(res);
            }).catch(error => {
                alert(error.message);
            })
        } 
    }

    return (
        <div className="container">
            <div className="row" id="formInformation">
                <div className="col-lg-3 col-md-3 col-sm-12"></div>
                <div className="col-lg-6 col-md-6 col-sm-12">
                    <div className="w3-row" style={{ marginBottom: 20 }}>
                        <h1 className="w3-center my_title_1">INFORMATIONS PERSONNELLES</h1>
                    </div>
                    <div className="formContainer">
                        <form onSubmit={(event) => formSubmit(event)}>
                            <label className="myLabel" id="civilite">Civilité{requireAsterix}</label><br />
                            <select className="my-form-control" onChange={(event) => setCivilite(event.currentTarget.value)} id="civilite">
                                <option value={null}>Choississez la civilité</option>
                                <option value={"M."}>M. (monsieur)</option>
                                <option value={"Mme."}>Mme (madame)</option>
                                <option value={"Mlle."}>Mlle. (mademoiselle)</option>
                            </select>

                            <label className="myLabel">Nom{requireAsterix}</label><br />
                            <input type="text" onKeyUp={(event) => setNom(event.currentTarget.value)} required className="my-form-control" id="nom" />

                            <label className="myLabel">Prenom{requireAsterix}</label><br />
                            <input type="text" onKeyUp={(event) => setPrenom(event.currentTarget.value)} required className="my-form-control" id="prenom" />

                            <label className="myLabel">Adresse{requireAsterix}</label><br />
                            <input type="text" onKeyUp={(event) => setAdresse(event.currentTarget.value)} required className="my-form-control" id="adresse" />

                            <label className="myLabel">Code postal{requireAsterix}</label><br />
                            <input type="number" onKeyUp={(event) => setCodePostal(event.currentTarget.value)} required className="my-form-control" id="codePostal" />

                            <label className="myLabel">Téléphone{requireAsterix}</label><br />
                            <input type="text" onKeyUp={(event) => setPhone(event.currentTarget.value)} required className="my-form-control" id="phone" />

                            <input type="submit" className="my-btn-form c-green" value="Terminer" />
                        </form>
                    </div>
                </div>
                <div className="col-lg-3 col-lg-3 col-sm-12"></div>
            </div>
            <div className="row" id="confirmationMsg" style={{ display: "none" }}>
                <div className="col-lg-3 col-lg-3 col-sm-12"></div>
                <div className="col-lg-6 col-md-6 col-sm-12">
                    <div className="w3-block formContainer">
                        <center><img src={iconSecure} alt="secureIcon" style={{ width: 100, height: "auto" }} /></center>
                        <h2 className="w3-center">Confirmation de votre inscription</h2>
                        <div className="w3-margin-bottom">
                            <p style={{ fontSize: "medium" }}>Le lien de confirmation de votre compte est envoyé dans votre adresse email <strong>{user.email}</strong></p>
                        </div>
                    </div>
                </div>
                <div className="col-lg-3 col-lg-3 col-sm-12"></div>
            </div>
        </div>
    );
}