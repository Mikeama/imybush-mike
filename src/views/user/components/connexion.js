import React, { useState, useEffect } from 'react';
import "../css/connexion.css";
import Axios from 'axios';
import { useAlert } from 'react-alert';
import { useDispatch } from 'react-redux';
import { loggingUser } from "../../../my_redux/actions/logginUser";
import $ from "jquery";

const requireAsterix = <span style={{ color: "rgb(248, 133, 1)" }}>*</span>;

export default function Connexion(props) {

	const alert = useAlert();
	const dispatch = useDispatch();

	////////////////////state///////////////////////////////////
	const [errorMsg, setErrorMsg] = useState("");
	const [passwordForgotUrl, setPasswordForgotUrl] = useState("#");
	const [emailOrUsername, setEmailOrUsername] = useState("");
	const [password, setPassword] = useState();
	//////////////////////////////////////////////////////////////

	///////////INIT////////////////////////////////////////////////
	const init = () => {
		//passwordForgotUrl
		setPasswordForgotUrl("#");
	}

	////////////////////////////////////////////////////////////////
	const formOnSubmit = (e) => {
		e.preventDefault();
		$("#submitBtn").val("chargement...");
		Axios.post(process.env.REACT_APP_BACKEND_URL + "/user/login", {
			username: emailOrUsername,
			email: emailOrUsername,
			password
		}).then(res => {
			dispatch(loggingUser({ username: res.data.success.username, id:res.data.success.id, token: res.data.success.token, name: res.data.success.name }));
			window.location.assign(localStorage.getItem('lastUrl') ? localStorage.getItem('lastUrl') : "/front");
		}).catch(err => {
			if (err.message.indexOf('401') > 0) {
				setErrorMsg("Nom d'utilisateur ou mot de passe incorrect.");
				$("#submitBtn").val("Connexion");
				$(".errorContent").fadeIn();
			} else {
				alert.error(err.message + "\nError 404. Veuillez verifier votre accès à l'internet.");
			}
		});
	}

	//////////////////////in mount and in update component ///////////////
	useEffect(() => {
		init();
		console.log(props.history);
	});
	/////////////////////////////////////////////////////////////////////

	return (
		<div className="container">
			<div className="w3-display-container" style={{ width: "100%", height: 600 }}>
				<div className="loginContent w3-display-topmiddle">
					<div className="headerLoginContent">
						<h1 className="my_title_1">CONNEXION</h1>
					</div>
					<div className="bodyLoginContent">
						<div className="errorContent" style={{ display: "none" }}>{errorMsg}</div>
						<form id="loginForm" onSubmit={(e) => formOnSubmit(e)}>
							<label className="myLabel" form="mailOrUsername">Adresse mail ou Pseudo{requireAsterix}</label><br />
							<input type="text" required id="mailOrUsername" onKeyUp={(event) => setEmailOrUsername(event.currentTarget.value)} className="my-form-control" placeholder="Email ou Pseudo" />
							<label className="myLabel" form="password">Mot de passe{requireAsterix}</label><br />
							<input type="password" required id="password" onKeyUp={(event) => setPassword(event.currentTarget.value)} className="my-form-control" placeholder="Mot de passe" />
							<div className="w3-display-container" style={{ marginBottom: 40 }}>
								<div className="w3-display-right">
									<a href={passwordForgotUrl} style={{ fontSize: "small" }}>Mot de passe oublié ?</a>
								</div>
							</div>
							<input type="submit" className="my-btn-form c-green" id="submitBtn" value="Connexion" />
							<div className="line w3-display-container">
								<div className="orLine w3-display-topmiddle">ou</div>
							</div>
							<button onClick={() => { window.location.assign("/front/registration") }} className="my-btn-border c-orange-border">S'incrire</button>
						</form>
					</div>
				</div>
			</div>
		</div>
	);
}