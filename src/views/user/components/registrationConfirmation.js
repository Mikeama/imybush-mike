import React, { useEffect } from "react";
import Axios from "axios";
import { useSelector, useDispatch } from "react-redux";
import { loggingUser } from "../../../my_redux/actions/logginUser";
import { deleteTemp } from "../../../my_redux/actions/temp";


export default function RegistrationConfirmation(props) {

    const user = useSelector(state => state.temp);

    const dispatch = useDispatch();

    useEffect(() => {
        const params = props.match.params;
        Axios.get(process.env.REACT_APP_BACKEND_URL + "/user/checkConfirmation/" + params.token).then(res => {
            if (res.data.confirmation === true) {
                Axios.post(process.env.REACT_APP_BACKEND_URL + "/user", user)
                    .then(res => {
                        // save on redux the user information
                        dispatch(loggingUser({
                            username: res.data.success.username,
                            id: res.data.success.id,
                            token: res.data.success.token
                        }));
                        dispatch(deleteTemp());
                        window.location.assign("/");
                    }).catch(err => {
                        dispatch(deleteTemp());
                        alert(err.message);
                    });
            } else {
                window.location.assign("/");
            }
        }).catch(err => {
            alert(err.message);
        })
    },[dispatch, props.match.params, user]);

    return (
        <></>
    )
}