import React from "react";

export default class Password {

    testLength = (password) => {
        if (password.length >= 8) {
            return true
        }
        return false;
    }

    testSpecial(password) {
        const regex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&*])(?=.{8,})/;
        if(regex.test(password)) {
            return true;
        }
        return false;
    }

    testMaj(password) {
        var upperCaseLetters = /[A-Z]/g;
        if (password.match(upperCaseLetters)) {
            return true;
        }
        return false;
    }

    testMin(password) {
        var lowerCaseLetters = /[a-z]/g;
        if (password.match(lowerCaseLetters)) {
            return true;
        }
        return false;
    }

    testNumber(password) {
        var numbers = /[0-9]/g;
        if (password.match(numbers)) {
            return true;
        }
        return false;
    }

    passwordEfficacite = (Password) => {
        var efficacite = null;
        var isValid = false;
        var password = Password.trim();
        if (this.testLength(password) && this.testMaj(password) && this.testMin(password) && this.testNumber(password) && this.testSpecial(password)) {
            isValid = true;
            efficacite = <span className="okMessageForm">Fort</span>;
        } else if (this.testLength(password) && this.testMaj(password) && this.testMin(password) && (this.testNumber(password) || this.testSpecial(password))) {
            isValid = true;
            efficacite = <span className="secondaryMessageForm">Moyenne</span>;
        } else {
            isValid = false;
            efficacite = <span className="dangerMessageForm">Faible</span>;
        }

        return { message: efficacite, isValid: isValid };
    }
}