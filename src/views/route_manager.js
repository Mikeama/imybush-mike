import React, { Suspense } from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Error from "./error";
import PrivateRoute from "../security/userPrivateRoute";


///////////Components///////////////////////////////////////

/*********Home*************/
const Home = React.lazy(() => import("./home/home"));

/**********User************/
const Connexion = React.lazy(() => import("./user/components/connexion"));
const Registration = React.lazy(() => import("./user/components/inscription"));
const MonCompte = React.lazy(() => import("./user/components/monCompte/monCompte"));
const InformationPersonnelle = React.lazy(() => import("./user/components/informationPersonnelle"));
const RegistrationConfirmation = React.lazy(() => import("./user/components/registrationConfirmation"));
const RegistrationChangeEmail = React.lazy(()=> import("./user/components/registrationChangeEmail"));

/*********Produit*************/
const NewProduit = React.lazy(() => import("./produit/components/newProduit"));
const DetailProduit = React.lazy(() => import("./produit/components/detailProduit"));
const UserProduit = React.lazy(() => import("./produit/components/userProduit"));
const TrouverProduit = React.lazy(() => import("./produit/components/trouverProduit"));
const Modifierproduit = React.lazy(() => import("./produit/components/modificationProduit"));
/////////////////////////////////////////////////////////////////////////////////////

class Content extends React.Component {
    render() {
        return (
            <Suspense fallback={null}>
                <Switch>
                    {/* Declare your routes here */}

                    {/* Home routes */}
                    <Route path="/front" exact component={Home} />

                    {/* User routes */}
                    <Route path="/front/login" exact component={Connexion} />
                    <Route path="/front/registration" exact component={Registration} />
                    <Route path="/front/registration/information_personnelle" exact component={InformationPersonnelle} />
                    <Route path="/front/registration/confirmation/:token" exact component={RegistrationConfirmation} />
                    <Route path="/front/editEmail/confirmation/:token" exact component={RegistrationChangeEmail} />
                    <PrivateRoute path="/front/mon_compte" exact component={MonCompte} />

                    {/* produit routes */}
                    <Route path="/front/detail_produit/:id" exact component={DetailProduit} />
                    <PrivateRoute path="/front/proposer_produit" exact component={NewProduit} />
                    <Route path="/front/vendeur_produits/:id" exact component={UserProduit} />
                    <Route path="/front/trouver_produit" exact component={TrouverProduit} />
                    <PrivateRoute path="/front/modifier_produit/:id" exact component={Modifierproduit} />

                    {/*error front*/}
                    <Route component={Error} />
                </Switch>
            </Suspense>
        );
    }
}

export default Content;