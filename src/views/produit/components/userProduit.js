import React, { useState, useEffect } from "react";

import InformationUser from "../../../components/informationUser/informationUser";
import ListProduit from "../../../components/listProduit/listProduit";
import Pagination from "../../../components/pagination/pagination";
import "../css/userProduit.css";
import $ from "jquery";

export default function UserProduit(prop) {

    const [nameUser, setNameUser] = useState("");
    const [photoUser, setPhotoUser] = useState("");
    const [adresseUser, setAdresseUser] = useState("");
    const [phoneUser, setPhoneUser] = useState("");
    const [emailUser, setEmailUser] = useState("");
    const [listProduit, setListProduit] = useState([]);

    //information User
    const buildInformationUser = () => {
        setNameUser("Vendeur");
        setPhoneUser("123 123 123 1232");
        setPhotoUser("");
        setEmailUser("mail@mail.com");
        setAdresseUser("Mon Adresse");
    }

    //listeProduit
    const buildListProduit = async () => {
        //simulation
        const delay = ms => new Promise(res => setTimeout(res, ms))
        await delay(1000);
        setListProduit([
            {
                image: "/image/pomme.jpg",
                name: "Pomme",
                prix: "00.00",
                dateDebut: "12 Novembre 2019",
                dateFin: "20 Janvier 2020",
                adresse: "Mon Adresse",
                phone: "123 156 2321",
                status: {
                    statusText: "En stock",
                    statusValue: "success"
                }
            },
            {
                image: "/image/melon.jpg",
                name: "Melon",
                prix: "00.00",
                dateDebut: "12 Novembre 2019",
                dateFin: "20 Janvier 2020",
                adresse: "Mon Adresse",
                phone: "123 156 2321",
                status: {
                    statusText: "En stock",
                    statusValue: "success"
                }
            },
            {
                image: "/image/poire.jpg",
                name: "Poire",
                prix: "00.00",
                dateDebut: "12 Novembre 2019",
                dateFin: "20 Janvier 2020",
                adresse: "Mon Adresse",
                phone: "123 156 2321",
                status: {
                    statusText: "En stock",
                    statusValue: "success"
                }
            }
        ]);
    }

    const onChangePage = (page) => {
        alert(page);
    }

    useEffect(() => {
        buildInformationUser();
        buildListProduit();
        $(document).ready(() => {
            $(".paginationContainer").width($(".userProduitContainer").width());
            $(window).resize(() => {
                $(".paginationContainer").width($(".userProduitContainer").width());
            });
        });
    })

    return (
        <div className="userProduitContainer container" style={{ marginBottom: 100 }}>
            <div className="w3-row" style={{ marginBottom: 60 }}>
                <div className="w3-col responsive" style={{ width: "55%" }}>
                    <div className="localisationContainerUserProduit">
                        <h1 className="titleLocalisationUserProduit">LOCALISATION</h1>
                        <div className="localisationContentUserProduit" id="googleMapUserProduit">
                        </div>
                    </div>
                </div>
                <div className="w3-col responsive" style={{ width: "5%", opacity: 0 }}>crack</div>
                <div className="w3-col responsive" style={{ width: "40%" }}>
                    <InformationUser hide={true} name={nameUser} photoUser={photoUser} adresse={adresseUser} phone={phoneUser} email={emailUser} />
                </div>
            </div>
            <div className="UserProduitListContainer">
                <div className="w3-display-container" style={{ width: "100%", height: 20, marginTop: 40, marginBottom: 35 }}>
                    <div className="w3-display-topleft">
                        <h1 className="titleListUserProduit">LISTE DES PRODUITS</h1>
                    </div>
                    <div className="w3-display-topright">
                        <div className="sortInputContainer">
                            <label style={{ fontSize: "small", fontWeight: "normal" }}>Tri:</label>
                            <select className="selectInputSort">
                                <option>Plus récents</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div className="listProduitContainer">
                    {
                        listProduit.map((useProduit, key) => (
                            <ListProduit key={key} data={useProduit} />
                        ))
                    }
                    <div className="paginationContainer" style={{ marginTop: 40, position: "absolute" }}>
                        <div className="w3-display-container">
                            <div className="w3-display-middle">
                                <Pagination count={10} onChange={onChangePage} />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}