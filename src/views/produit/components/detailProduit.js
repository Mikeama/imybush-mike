import React from "react"
import Flickity from "flickity";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import InformationUser from "../../../components/informationUser/informationUser";
import CardProduit from "../../../components/cardProduit/carProduit";
import ResponsiveGrid from "../../../components/responsiveGrid/responsiveGrid";
import "../../../assets/css/flickity.min.css";
import "../css/detailProduit.css";
import "../js/detailProduitScript";


export default class DetailProduit extends React.Component {

    state = {
        nomProduit: "Papaye",
        prixProduit: 1000,
        statusProduit: {
            value: "En stock",
            status: "success"
        },
        datePostedProduit: "Le 12 Novembre 2019",
        dateDispo: {
            debut: "12 Novembre 2010",
            fin: "01 Janvier 2010"
        },
        adresseProduit: "Mon adresse",
        phone: "238 9823 91829",
        descriptionProduit: "We have created a fictional band website. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.",
        otherProduitData: [
            {
                id: 1,
                name: "Tomate",
                prix: "00.00",
                photo: "/image/tomate.jpg"
            },
            {
                id: 2,
                name: "Melon",
                prix: "00.00",
                photo: "/image/melon.jpg"
            },
            {
                id: 3,
                name: "Pomme",
                prix: "00.00",
                photo: "/image/pomme.jpg"
            },
            {
                id: 4,
                name: "Poire",
                prix: "00.00",
                photo: "/image/poire.jpg"
            }

        ]
    }

    init = () => {
        new Flickity('.flickityDetailProduit', {
            wrapAround: true,
            freeScroll: true,
            lazyLoad: true
        });
    }

    builOtherProduit = () => {
        var components = [];
        this.state.otherProduitData.forEach((produit) => {
            components.push(<CardProduit style={{
                marginBottom: "4%"
            }} imageOrPlaceholder={produit.photo} altImage={produit.name} imageLazyload={produit.photo} nameProduit={produit.name} prixProduit={produit.prix}/>);
        });
        return {
            contents: components,
            spaceWidthInPercent: 2
        };
    }

    componentDidMount() {
        this.init();
    }

    render() {
        return (
            <div className="container" style={{ marginBottom: 40, fontSize: "small" }}>
                <div id="test"></div>
                <div className="w3-row">
                    <div className="w3-col responsive" style={{ width: "55%" }}>
                        <div class="flickityDetailProduit" style={{ width: "100%" }}>
                            <div class="carousel-cell"></div>
                            <div class="carousel-cell"></div>
                            <div class="carousel-cell"></div>
                        </div>
                        <div className="informationContainer" style={{ marginTop: 40 }}>
                            <div className="w3-row" style={{ width: "100%", marginBottom: 20 }}>
                                <div className="w3-col" style={{ width: "75%" }}>
                                    <h1 className="nameProduit">{this.state.nomProduit}</h1>
                                    <div className="prix">{this.state.prixProduit}€</div>
                                    <div className="datePosted">{this.state.datePostedProduit}</div>
                                </div>
                                <div className="w3-col" style={{ width: "25%" }}>
                                    <div className={"status " + this.state.statusProduit.status}>{this.state.statusProduit.value}</div>
                                </div>
                            </div>
                            <div className="dateDispoContainer" style={{ marginBottom: 20 }}>
                                <label style={{ fontSize: "small" }}>Période de disponibilité:</label><br />
                                <div style={{ fontSize: "small" }}><span className="dateDispo">{this.state.dateDispo.debut}</span>&nbsp;jusqu'au&nbsp;<span className="dateDispo">{this.state.dateDispo.fin}</span></div>
                            </div>
                            <div className="addresseAndNumContainer" style={{ marginBottom: 20 }}>
                                <div><FontAwesomeIcon icon="home" />&nbsp;{this.state.adresseProduit}</div>
                                <div><FontAwesomeIcon icon="phone" />&nbsp;{this.state.phone}</div>
                            </div>
                            <div className="descriptionContainer" style={{ marginBottom: 30 }}>
                                <label>Description</label><br />
                                <p style={{ textAlign: "justify" }}>{this.state.descriptionProduit}</p>
                            </div>
                            <div className="localisationContainer">
                                <label>Localisation</label><br />
                                <div className="localisationContent"></div>
                            </div>
                        </div>
                    </div>
                    <div className="w3-col responsive" style={{ width: "5%", opacity: 0 }}>crack</div>
                    <div className="w3-col responsive" style={{ width: "40%" }}>
                        <InformationUser name={"NomVendeur"} adresse={"Mon Adresse"} phone={"01 232 233 323"} email={"vendeur@vendeur.com"} />
                        <h1 className="otherAnnonceTitle">
                            
                            <span style={{ color: "rgb(0, 175, 0)" }}>CES ANNONCES</span>&nbsp;PEUVENT VOUS INTERESSER
                        </h1>
                        <ResponsiveGrid row={2} col={2} data={this.builOtherProduit()} />
                    </div>
                </div>
            </div>
        );
    }
}