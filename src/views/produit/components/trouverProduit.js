import React, { useState, useEffect } from "react";
import InfiniteScroll from "react-infinite-scroll-component";
import Vendeur from "../../../components/vendeurList/vendeurList";
import ListProduit from "../../../components/listProduitInFindProduct/listproduit";
import "../css/trouverProduit.css";
import $ from "jquery";
import Pagination from "../../../components/pagination/pagination";
import Map from "../../../components/googleMap/trouverProduitMap";

export default function TouverProduit(prop) {

    const [vendeurs, setVendeurs] = useState([]);
    const [resultatPropose, setResultatPropose] = useState("Les Abymes (100 résultats)");
    const [listProduit, setLisProduit] = useState([]);

    const BuildVendeurList = (ville) => {

        //version_test
        setVendeurs(
            [
                {
                    nom: "Françoise Freuville",
                    produit: "Betterave",
                    address: "Mon Adresse",
                    date: "Le 12 Janvier 2020",
                    pdp: "https://scontent.ftnr2-1.fna.fbcdn.net/v/t1.0-9/32260552_173282756838549_1148911637239955456_n.jpg?_nc_cat=105&_nc_sid=09cbfe&_nc_eui2=AeEzhwoT1ST9DhjYoXLOhRA7yn-wS4phIJDKf7BLimEgkIQc5Bz9UoUKwI6ER_yZIWVWpwqubGoo2pDzqFeEtZy1&_nc_ohc=KNbjDTga6ncAX8g_Wv9&_nc_ht=scontent.ftnr2-1.fna&oh=88da4fafd1a0601c4dc2244ac49f89cb&oe=5F39FD55"
                },
                {
                    nom: "Francoise Fillon",
                    produit: "Thon",
                    address: "Mon Adresse",
                    date: "Le 05 Janvier  2020",
                    pdp: "https://scontent.ftnr2-1.fna.fbcdn.net/v/t1.0-9/16388069_102642870253169_1939730383986389364_n.jpg?_nc_cat=103&_nc_sid=09cbfe&_nc_eui2=AeGSCay5E2FL9-uf797f1lSrJbbk7u2FB1AltuTu7YUHUFQLJSdIbGMWH-6cDWvEaGT9saZ2PGy5pzAW5vIoaakC&_nc_ohc=xdRlmz9A3jMAX9MCTwv&_nc_ht=scontent.ftnr2-1.fna&oh=80f140c93d5f57a51ceea2f1c4292b78&oe=5F3A05DF"
                },
                {
                    nom: "Francoise Filliez",
                    produit: "Sardines",
                    address: "Mon Adresse",
                    date: "Le 21 Décembre 2019",
                    pdp: "https://scontent.ftnr2-1.fna.fbcdn.net/v/t31.0-8/29060664_409525102829631_311897519981518166_o.jpg?_nc_cat=107&_nc_sid=09cbfe&_nc_eui2=AeHiBMVi9VySkz3Mdmmbh_fVkSuxN-k0yo2RK7E36TTKjW83Vr0-gGd37HMiN8DJI24yvk8omf-YH8FjAfJC-W2a&_nc_ohc=NN1puhiGmkAAX_6PAHf&_nc_ht=scontent.ftnr2-1.fna&oh=26fdc8df29360a79570653b772df9ab2&oe=5F3BACA9"
                },
                {
                    nom: "Anne-Françoise Foccroulle",
                    produit: "filet de poisson",
                    address: "Mon Adresse",
                    date: "Le 20 Décembre 2019",
                    pdp: "https://scontent.ftnr2-1.fna.fbcdn.net/v/t1.0-9/49209981_1178132109000432_5052424657420943360_o.jpg?_nc_cat=109&_nc_sid=09cbfe&_nc_aid=0&_nc_eui2=AeFGoaCzcfhYfg2-K6S-bK2JtecqGNdvgAa15yoY12-ABsRSlPXL2-B1TQAJcnx7TlbcXCRVF4BDeEMzGVivcQTz&_nc_ohc=uavsSd-yme8AX_r3wmv&_nc_ht=scontent.ftnr2-1.fna&oh=b3c264dc222a0746dbe31379663c067b&oe=5F3B46D2"
                },
                {
                    nom: "Margeuritte Mulkens",
                    produit: "Carpe",
                    address: "Mon Adresse",
                    date: "Le 30 Novembre 2019",
                    pdp: "https://scontent.ftnr2-1.fna.fbcdn.net/v/t1.0-9/104187349_1034272017034336_6770230262303591957_n.jpg?_nc_cat=106&_nc_sid=09cbfe&_nc_eui2=AeEd7VQWhBNuSWCRFU4TOYT_6FQY5b79KyzoVBjlvv0rLAQXQeX61qqFef-jL8z7oQ7ebrPrtshMEvDqErLEhilU&_nc_ohc=WSs7BfzLGs4AX9N4TSZ&_nc_ht=scontent.ftnr2-1.fna&oh=890139bc786a3646acd4a30ebdad58ec&oe=5F3AF4B8"
                },
                {
                    nom: "Lucie",
                    produit: "Dattier",
                    address: "Mon Adresse",
                    date: "Le 32 Novembre 2019",
                    pdp: "https://scontent.ftnr2-1.fna.fbcdn.net/v/t1.0-9/32235586_110464856495687_5949966470304759808_n.jpg?_nc_cat=106&_nc_sid=174925&_nc_eui2=AeFD_sQ_sYZj3ZhA9JE3xPcIP7ir78n5CBs_uKvvyfkIG4dmIzTzw2xZRNkOw_aj9ivGP-E_IJjoy1Tx0hkF4T1Z&_nc_ohc=23K37oZUbGMAX_qrseH&_nc_ht=scontent.ftnr2-1.fna&oh=e6cae5391b6391d37d6bfd2a9705e951&oe=5F3A9062"
                },
                {
                    nom: "Mariette",
                    produit: "Betterave",
                    address: "Mon Adresse",
                    date: "Le 25 Novembre 2019",
                    pdp: "https://scontent.ftnr2-1.fna.fbcdn.net/v/t31.0-8/29871523_10213544747591197_7602784513610742266_o.jpg?_nc_cat=111&_nc_sid=174925&_nc_eui2=AeG4pao9Z40cJa3Rn59Dwe46Tv5P5oN6LOdO_k_mg3os53PbQWjTles3-uVLfoTOMYuWTdZsk0kT1ZG-EYuIfnck&_nc_ohc=tEGPFd7uZHIAX8wrw03&_nc_ht=scontent.ftnr2-1.fna&oh=f5fff1f589a6ced2510b788b8c7257ac&oe=5F3A86B6"
                }
            ])
    }

    const buildListProduit = async (coordonne) => {
        setLisProduit([
            {
                image: "https://scontent.ftnr4-1.fna.fbcdn.net/v/t1.0-9/116757230_1199872407059860_7211383040027528153_n.jpg?_nc_cat=107&_nc_sid=730e14&_nc_eui2=AeGtkkxLtKfTEKhSw6JVkDYzae5g6LGuIRdp7mDosa4hF8vNmOb8jkyR0CVpfRHvdHp6ZCe6Lgf6wzXDhzzdbLzD&_nc_ohc=uc9zCDyHbckAX9ge7mK&_nc_ht=scontent.ftnr4-1.fna&oh=e8506bd47c5151e5478a294bfb18dc94&oe=5F55A1BA",
                name: "Pomme",
                prix: "00.00",
                dateDebut: "12 Novembre 2019",
                dateFin: "20 Janvier 2020",
                adresse: "Mon Adresse",
                phone: "123 156 2321",
                nameVendeur: "vendeur Nom",
                datePost: "Le 15 Novembre 2019",
                email: "vendeur@vendeur.com"
            },
            {
                image: "https://scontent.ftnr4-1.fna.fbcdn.net/v/t1.0-9/116909125_612632222605199_565742495207017991_o.jpg?_nc_cat=104&_nc_sid=8bfeb9&_nc_eui2=AeEu5Wx77I2tcv5ocXX7A3WcvWvNNYEPCM-9a801gQ8Iz6X1kR2g5-ZvbsMxlhJq2Ei4iKiSS1kVetuMx3m8-YRv&_nc_ohc=O6qUrAEX5UEAX_Ymm12&_nc_ht=scontent.ftnr4-1.fna&oh=cf0296c67168dc6c86a7f2267734ca30&oe=5F562633",
                name: "Melon",
                prix: "00.00",
                dateDebut: "12 Novembre 2019",
                dateFin: "20 Janvier 2020",
                adresse: "Mon Adresse",
                phone: "123 156 2321",
                nameVendeur: "vendeur Nom",
                datePost: "Le 15 Novembre 2019",
                email: "vendeur@vendeur.com"
            },
            {
                image: "https://scontent.ftnr4-1.fna.fbcdn.net/v/t1.0-9/116444536_3692281594134814_4958176434869766908_n.jpg?_nc_cat=111&_nc_sid=8bfeb9&_nc_eui2=AeEaxxl4WtzpAXIhU-ViNeuRPjP-djKeGF4-M_52Mp4YXtiG8PwYxabYYomgfepQvMaRW9ng3LI00lOwVSzU1eX5&_nc_ohc=NV6BqZppKDEAX9bDZ_k&_nc_ht=scontent.ftnr4-1.fna&oh=c4fa98bbe5049501a61515233573f8ec&oe=5F551399",
                name: "Poire",
                prix: "00.00",
                dateDebut: "12 Novembre 2019",
                dateFin: "20 Janvier 2020",
                adresse: "Mon Adresse",
                phone: "123 156 2321",
                nameVendeur: "vendeur Nom",
                datePost: "Le 15 Novembre 2019",
                email: "vendeur@vendeur.com"
            }
        ]);
    }

    const onChangePage = (page) => {
        alert(page);
    }


    useEffect(() => {
        BuildVendeurList(); //default Building
        buildListProduit(); //default Building
        $(document).ready(() => {
            responsive();
            $(window).resize(() => {
                responsive();
            });

            function responsive() {
                $("#test").html($(window).width());
                var windowWidth = $(window).width();
                if (windowWidth < 983) {
                    $(".localisationContainer").css("height", "400px");
                    $(".triInputProduitProposeContainer").css("margin-top", "30px");
                    $(".listProduitProposeContent").css("margin-top", "140px");
                } else {
                    $(".localisationContainer").css("height", "570px");
                    $(".triInputProduitProposeContainer").css("margin-top", "-7px");
                    $(".listProduitProposeContent").css("margin-top", "60px");
                }
                $(".headerProduitPropose").width($(".touverProduitContainer").width());
            }
        });
    }, []);

    return (
        <div className="container touverProduitContainer" style={{ marginBottom: 60 }}>
            <div className="w3-row" style={{ marginBottom: 55 }}>
                <div className="w3-col responsive" style={{ width: "48%" }}>
                    <h1 className="titleLocalisationTrouverProduit">LOCALISATION</h1>
                    <div className="localisationContainer">
                        <Map />
                    </div> 
                </div>
                <div className="w3-col responsive" style={{ width: "9%", opacity: 0 }}>crack</div>
                <div className="w3-col responsive" style={{ width: "43%" }}>
                    <div className="listVendeurTrouverProduitContainer">
                        <div className="listVendeurTrouverProduitTitle">
                            <h1 className="TitleListProduitElement">LISTE DES VENDEURS</h1>
                        </div>
                        <div className="listVendeurTrouverProduitBody">
                            <InfiniteScroll
                                dataLength={vendeurs.length}
                                next={null}
                                hasMore={false}
                                height={550}
                            >
                                {vendeurs.map((vendeur, key) => (
                                    <Vendeur key={key} pdp={vendeur.pdp} name={vendeur.nom} prenom={""} produit={vendeur.produit} adresse={vendeur.address} date={vendeur.date} />
                                ))}
                            </InfiniteScroll>
                        </div>
                    </div>
                </div>
            </div>
            <div style={{opacity: 0}}>crack</div>
            <div className="headerProduitProposeContainer" style={{ position: "relative" }}>
                <div className="headerProduitPropose" style={{ width: "100%", position: "absolute" }}>
                    <div style={{ float: "left" }}>
                        <h1 style={{ fontSize: "large", margin: 0 }}>
                            <span className="tileListProduitPropose"><span style={{ color: "rgb(12, 168, 25)" }}>CES ANNONCES</span> PEUVENT VOUS INTERESSER : </span>
                            <span className="resultatPropose">{resultatPropose}</span>
                        </h1>
                    </div>
                    <div className="triInputProduitProposeContainer w3-row">
                        <div className="w3-col" style={{ width: 30 }}>
                            <label style={{ fontSize: "small", fontWeight: "normal" }}>Tri : </label>
                        </div>
                        <div className="w3-rest">
                            <select style={{ width: "100%", fontSize: "small", border: "none", background: "transparent" }}>
                                <option>Plus récentes</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div className="listProduitProposeContent" style={{ width: "100%" }}>
                {
                    listProduit.map((produit, key) => (
                        <ListProduit key={key} data={produit} />
                    ))
                }
            </div>
            <div style={{marginTop: 40}}>
                <center>
                    <div className="paginationContainerFindProduct" style={{ width: 390 }}>
                        <Pagination count={10} onChange={onChangePage} />
                    </div>
                </center>
            </div>
        </div>
    );
}