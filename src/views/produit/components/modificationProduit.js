import React, { useState, useEffect, useCallback } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import filterListDropdown from "../../../assets/js/filterDropdown";
import ReactDropzone from "react-dropzone";
import "../css/newProduit.css";
import $ from "jquery";
import Axios from "axios";
import Map from "../../../components/myMap";
import AutoComplete from "../../../components/autoComplete/autoComplete";
import { useSelector } from "react-redux";
import { useAlert } from "react-alert";
import swal from 'sweetalert';

const url = process.env.REACT_APP_BACKEND_URL;

const requireAsterix = <span style={{ color: "rgb(248, 133, 1)" }}>*</span>;

export default function UpdatProduit(props) {

    const alert = useAlert();

    const user = useSelector(state => state.logger.current_user);

    ////////////////////////STATE/////////////////////////
    const [id, setId] = useState("");
    const [dateFin, setdateFin] = useState("");
    const [dateDebutOld, setDateDebutOld] = useState('');
    const [dateDebut, setDateDebut] = useState("");
    const [dispoStatus, setDispoStatus] = useState({
        valid: false,
        message: ""
    });
    const [radioBtn, setRadioBtn] = useState({
        horsStock: false,
        enStock: false
    });
    const [categorieFilter, setCategorieFilter] = useState({
        placeholder: "Séléctionnez ici la catégorie",
        value: null,
        messageError: ""
    });
    const [produitFilter, setProduitFilter] = useState({
        placeholder: "Séléctionnez ici le produit",
        value: null,
        messageError: ""
    });
    const [titreAnnonce, setTitreAnnonce] = useState({
        value: "",
        messageError: ""
    })
    const [description, setDescription] = useState({
        value: "",
        messageError: ""
    });
    const [prix, setPrix] = useState({
        value: 0,
        messageError: ""
    });
    const [email, setEmail] = useState({
        value: "",
        valid: false,
        messageError: ""
    });
    const [tel, setTel] = useState({
        value: "",
        valid: false,
        messageError: ""
    });
    const [photos, setPhotos] = useState([]);
    const [photosAnnonce, setPhotosAnnonce] = useState([]);
    const [elementErrorPhotos, setElementErrorPhotos] = useState({
        valid: false,
        messageError: []
    });
    const [localisationCoor, setLocalisationCoor] = useState({
        lat: "",
        lng: ""
    });
    const [produits, setProduits] = useState([]);
    const [categories, setCategories] = useState([]);
    const [btnName, setBtnName] = useState("Enregister");
    const [stock, setStock] = useState("En stock")
    ///////////////////////////////////////////////////////

    ///////////Localisation///////////////////////////////
    const localisationChoosen = (coor) => {
        if (coor.hasOwnProperty('lat')) {
            setLocalisationCoor({
                lat: coor.lat,
                lng: coor.lng
            });
        } else {
            setLocalisationCoor({
                lat: coor.coordinates[1],
                lng: coor.coordinates[0]
            });
        }
    }
    /////////////////////////////////////////////////////

    ////////////////////CATEGORIE////////////////////////
    const categorieChoosen = (value, name, idList) => {
        alert.success("ok");
        setCategorieFilter({
            placeholder: name,
            value: value
        });
        Axios.get(url + "/categorie/" + value).then(res => {
            setProduits(res.data.produits);
            setProduitFilter({
                placeholder: "Séléctionnez ici le produit",
                value: null,
                messageError: ""
            })
        }).catch(err => {
            alert("Error network !!!");
        });
        filterListDropdown.showList(idList);
    }
    ////////////////////////////////////////////////////

    /////////////////PRODUIT////////////////////////////
    const produitChoosen = (value, name, idList) => {
        setProduitFilter({
            placeholder: name,
            value: value
        });
        filterListDropdown.showList(idList);
    }

    ///////////////////////////////////////////////////

    ///////////////EMAIL////////////////////////////////////////
    const emailKeyUp = (email) => {
        const regexEmail = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if (regexEmail.test(email)) {
            setEmail({
                value: email,
                valid: true,
                messageError: ""
            });
        } else {
            setEmail({
                value: "",
                valid: false,
                messageError: <span className="dangerMessageForm">Ce n'est pas un adresse mail</span>
            });
        }
    }
    //////////////////////////////////////////////////////////////////////

    ///////////////////////TELEPHONE//////////////////////////////////////
    const telKeyUp = (tel) => {
        const regexTel = /^\+?[0-9]+$/;
        if (regexTel.test(tel)) {
            setTel({
                value: tel,
                valid: true,
                messageError: ""
            });
        } else {
            setTel({
                value: "",
                valid: false,
                messageError: <span className="dangerMessageForm">Veuillez remplir correctement ce champ</span>
            });
        }
    }
    /////////////////////////////////////////////////////////////////////

    //////////////////////////TITREANNONCE//////////////////////////////////////
    const checkTitreAnnonce = (titreAnnonce) => {
        if (titreAnnonce !== "") {
            setTitreAnnonce({
                value: titreAnnonce,
                messageError: ""
            })
        } else {
            setTitreAnnonce({
                value: "",
                messageError: <span className="dangerMessageForm">Veuillez remplir correctement ce champ</span>
            })
        }
    }
    ////////////////////////////////////////////////////////////////////////////

    //////////////////////////DESCRIPTION//////////////////////////////////////
    const checkDescription = (description) => {
        if (description !== "") {
            setDescription({
                value: titreAnnonce,
                messageError: ""
            });
        } else {
            setDescription({
                value: "",
                messageError: <span className="dangerMessageForm">Veuillez remplir correctement ce champ</span>
            });
        }
    }
    ////////////////////////////////////////////////////////////////////////////

    //////////////////////////PRIX//////////////////////////////////////
    const checkPrix = (prix) => {
        if (prix !== "") {
            setPrix({
                value: prix,
                messageError: ""
            })
        } else {
            setPrix({
                value: "",
                messageError: <span className="dangerMessageForm">Veuillez remplir correctement ce champ</span>
            })
        }
    }
    ////////////////////////////////////////////////////////////////////////////

    ///////////////////Photo////////////////////////////////////////////
    const onDropFile = (files) => {
        responsiveImage();
        var tempFiles = [...photos];
        for (var i = 0; i < files.length; i++) {
            var photo = Object.assign(files[i], {
                preview: URL.createObjectURL(files[i])
            });
            tempFiles.push(photo);
        }
        setPhotos(tempFiles);
        setElementErrorPhotos({
            valid: true,
            messageError: []
        })
    }

    const onDropRejectedFile = (arg) => {
        let errors = arg[0].errors;
        console.log(errors);
        let message = [];
        errors.forEach(error => {
            if (error.code === "file-invalid-type") {
                message.push(<span className="dangerMessageForm">Le fichier doit être une image.</span>);
            } else if (error.code === "too-many-files") {
                message.push(<span className="dangerMessageForm">Un fichier à la fois</span>);
            } else {
                message.push(<span className="dangerMessageForm">{error.message}</span>);
            }
        });
        setElementErrorPhotos({
            valid: false,
            messageError: message
        });
    }

    const deletePhoto = (key) => {
        let CopyPhotos = [...photos];
        let tempArray = [];
        for (var i = 0; i < CopyPhotos.length; i++) {
            if (i !== key) {
                tempArray.push(CopyPhotos[i]);
            }
        }
        setPhotos(tempArray);
    }

    const deleteOldPhoto = (photo) => {
        const photoOld = photosAnnonce.filter(n => n !== photo);
        setPhotosAnnonce(photoOld);
    }
    /////////////////////////////////////////////////////////

    /////////////////Date_dispo///////////////////////////
    const dateKeyUp = (dateDebut1, dateFin1) => {
        console.log(dateDebut, dateFin1);
        if (dateDebut1 !== "" && dateFin1 !== "") {
            var Date1 = new Date(dateDebut1);
            var Date2 = new Date(dateFin1);
            var DebutOld = new Date(dateDebutOld);
            var now = new Date(new Date().toISOString().split("T")[0]);
            console.log(Date1 < Date2, Date1 >= DebutOld);
            if (Date1 <= Date2 && Date1 >= DebutOld && Date2 >= now) {
                setDispoStatus({
                    valid: true,
                    message: ""
                });
            } else if (Date2 < now && Date1 <= Date2) {
                setDispoStatus({
                    valid: false,
                    message: <span className="dangerMessageForm">L'annonce est expirée.</span>
                });
            } else {
                setDispoStatus({
                    valid: false,
                    message: <span className="dangerMessageForm">Erreur de logique</span>
                });
            }
        }
    }

    const buildAnnonceInformation = useCallback((annonce) => {
        //categorie and produit
        getProduitAndCategorie(annonce);

        //title
        $("#titleEditAnnonce").val(annonce.title);
        setTitreAnnonce({
            value: annonce.title,
            messageError: "",
        });

        //descirption
        $("#descriptionAnnonce").val(annonce.description);
        setDescription({
            value: annonce.description,
            messageError: ""
        });

        //price
        $("#priceEditAnnonce").val(parseFloat(annonce.price));
        setPrix({
            value: annonce.price,
            messageError: ""
        });

        //etat de stock
        if (annonce.etatStock === "En stock") {
            setRadioBtn({
                horsStock: false,
                enStock: true
            });
            setStock("En stock");
        } else if (annonce.etatStock === "Hors stock") {
            setRadioBtn({
                horsStock: true,
                enStock: false
            });
            setStock("Hors stock");
        }

        //date
        $("#dateDebut").val(annonce.disponibility.debut.split("T")[0]);
        setDateDebut(annonce.disponibility.debut.split("T")[0]);
        setDateDebutOld(annonce.disponibility.debut.split("T")[0]);
        $("#dateFin").val(annonce.disponibility.fin.split("T")[0]);
        setdateFin(annonce.disponibility.fin.split("T")[0]);

        //image 
        setPhotosAnnonce(annonce.images);
        responsiveImage();

        //localisation
        setLocalisationCoor({
            lat: annonce.localisation.lat,
            lng: annonce.localisation.lng
        });

        //email
        $("#emailEditAnnonce").val(annonce.email);
        setEmail({
            value: annonce.email,
            valid: true
        });

        $("#telephoneEditAnnonce").val(annonce.telephone);
        setTel({
            valid: true,
            value: annonce.telephone
        });

    }, []);

    const getProduitAndCategorie = async (annonce) => {
        var i = 0;
        const produitRes = await Axios.get(url + "/produit");
        const categorieRes = await Axios.get(url + "/categorie");
        const produit = [annonce.produit];
        for (i = 0; i < produitRes.data.length; i++) {
            if (!produitRes.data[i].annonces.includes(annonce._id)) {
                produit.push((produitRes.data[i]));
            }
        }
        setProduitFilter({
            placeholder: produit[0].designation,
            value: produit[0]._id,
            messageError: ""
        });
        setProduits(produit);

        const categorie = [annonce.categorie];
        for (i = 0; i < categorieRes.data.length; i++) {
            if (!categorieRes.data[i].annonces.includes(annonce._id)) {
                categorie.push(categorieRes.data[i]);
            }
        }
        setCategorieFilter({
            placeholder: categorie[0].designation,
            value: categorie[0]._id,
            messageError: ""
        });
        setCategories(categorie);
    }

    //////Submit///////////////
    const saveProduit = () => {
        if (produitFilter.value != null && categorieFilter.value != null && titreAnnonce.value !== "" && description.value !== "" && email.valid && tel.valid !== "" && prix.value !== "" && localisationCoor.lat !== "" && localisationCoor.lng !== "" && (photos.length > 0 || photosAnnonce.length > 0)) {
            const formData = new FormData();
            formData.append("categorie", categorieFilter.value);
            formData.append("produit", produitFilter.value);
            formData.append("title", titreAnnonce.value);
            formData.append("description", description.value);
            formData.append("disponibility", dateDebut + ";" + dateFin);
            formData.append("localisation", localisationCoor.lat + ";" + localisationCoor.lng)
            formData.append("etatStock", stock)
            photos.forEach(image => {
                formData.append("images", image);
            });
            formData.append("price", prix.value);
            formData.append("images", photos);
            formData.append("email", email.value);
            formData.append("telephone", tel.value);
            formData.append("user", user.id);
            formData.append("oldImages", photosAnnonce.toString());

            setBtnName("Chargement...");
            swal({
                title: "Confirmation",
                text: "Vous voulez vraiment enregister ces modifications ?",
                icon: "warning",
                buttons: {
                    cancel: {
                        text: "Annuler",
                        value: false,
                        visible: true
                    },
                    confirm: {
                        text: "Confirmer",
                        value: true
                    }
                }
            }).then((willUpate) => {
                if (willUpate) {
                    Axios.patch(url + "/annonce/" + id, formData, {
                        headers: {
                            "Content-Type": "multipart/form-data",
                            "Authorization": "Barer " + user.token
                        }
                    }).then(() => {
                        swal({
                            title: "Modification réussie.",
                            icon: "success",
                            button: {
                                text: "Suivant",
                                value: true
                            }
                        }).then(() => {
                            window.location.assign("/front/mon_compte");
                        });
                    }).catch(error => {
                        alert.error(error.message);
                    });
                } else {
                    setBtnName("Enregister");
                }
            });
        }
        else {
            swal({
                icon: "error",
                title: "Erreur",
                text: "Veuillez remplir tous les formulaires correctement !",
                button: {
                    text: ""
                }
            });
            checkTitreAnnonce(titreAnnonce.value);
            checkDescription(description.value);
            checkPrix(prix.value);
        }
    }

    //////////////////////////on_start_component////////////////////////////////
    useEffect(() => {
        setId(props.match.params.id);
        Axios.get(url + "/annonce/" + props.match.params.id).then(res => {
            buildAnnonceInformation(res.data);
        }).catch(err => { alert.error(err.message) });

        //jquery code
        $(document).ready(() => {
            responsiveImage()
            $(window).resize(() => {
                responsiveImage()
                var elementDropZoneContainerWidth = $(".imgContainer").width() - 4;
                $(".imgContainer").css('height', elementDropZoneContainerWidth + 'px');
                $(".imageContent").css({
                    "height": elementDropZoneContainerWidth + 'px',
                    "width": elementDropZoneContainerWidth + 'px'
                });
            });
            ///////////////////////////////////////////////////////////////////////
        });
    }, [alert, buildAnnonceInformation, props.match.params.id]);

    /////////////////////////////////////////////////////////////////////////////
    const responsiveImage = () => {
        $(function () {
            /////////////////////responsive_photo_container////////////////////////////
            var elementDropZoneContainerWidth = $(".imgContainer").width() - 5;
            $(".imgContainer").css('height', elementDropZoneContainerWidth + 'px');
            $(".imageContent").css({
                "height": elementDropZoneContainerWidth + 'px',
                "width": elementDropZoneContainerWidth + 'px'
            });
        });
    }
    return (
        <div className="container">
            <div className="row" style={{ marginBottom: 70 }}>
                <div className="col-lg-2 col-md-2 col-sm-12"></div>
                <div className="col-lg-8 col-md-8 col-sm-12">
                    <div className="addAnnonceContainer">
                        <div className="cardContainer">
                            <div className="headerCard">
                                Description du produit
                            </div>
                            <div className="bodyCard">
                                <label className="myLabel">Catégories{requireAsterix}</label><br />
                                <div className="dropdown" style={{ marginBottom: 20 }}>
                                    <button onClick={() => filterListDropdown.showList("categorieListNewAnnonce")} className="dropbtnAddAnnonce">
                                        <div className="placeholder annoncePlacehoder">{categorieFilter.placeholder}</div>
                                        <div className="chevronDown"><FontAwesomeIcon icon="chevron-down" /></div>
                                    </button>
                                    <div id="categorieListNewAnnonce" className="dropdown-content">
                                        <input type="text" placeholder="Recherche..." id="newAnnonceInput" className="myInput" onKeyUp={() => filterListDropdown.filterList("newAnnonceInput", "categorieListNewAnnonce")} />
                                        <div className="list-content">
                                            {
                                                categories.map((item, key) =>
                                                    <a key={key} onClick={() => categorieChoosen(item._id, item.designation, "categorieListNewAnnonce")} href={"#_Categories"}>{item.designation}</a>
                                                )
                                            }
                                        </div>
                                    </div>
                                </div>

                                <label className="myLabel">Produits{requireAsterix}</label><br />
                                <div className="dropdown" style={{ marginBottom: 20 }}>
                                    <button onClick={() => filterListDropdown.showList("produitListNewAnnonce")} className="dropbtnAddAnnonce">
                                        <div className="placeholder annoncePlacehoder">{produitFilter.placeholder}</div>
                                        <div className="chevronDown"><FontAwesomeIcon icon="chevron-down" /></div>
                                    </button>
                                    <div id="produitListNewAnnonce" className="dropdown-content">
                                        <input type="text" placeholder="Recherche..." id="prodiuitNewAnnonceInput" className="myInput" onKeyUp={() => filterListDropdown.filterList("prodiuitNewAnnonceInput", "produitListNewAnnonce")} />
                                        <div className="list-content">
                                            {
                                                produits.map((item, key) => (
                                                    <a key={key} onClick={() => produitChoosen(item._id, item.designation, "produitListNewAnnonce")} href={"#_Produits"}>{item.designation}</a>
                                                ))
                                            }
                                        </div>
                                    </div>
                                </div>

                                <label className="myLabel">Titre de l'annonce{requireAsterix}</label>{titreAnnonce.messageError}<br />
                                <input type="text" id="titleEditAnnonce" onKeyUp={(event) => setTitreAnnonce({ value: event.currentTarget.value })} className="my-form-control" />

                                <label className="myLabel">Description{requireAsterix}</label>{description.messageError}<br />
                                <textarea id="descriptionAnnonce" onKeyUp={(event) => setDescription({ value: event.currentTarget.value })} className="my-form-control" style={{ height: 100, resize: "none" }}>{description.value}</textarea>

                                <label className="myLabel">Prix{requireAsterix}</label>{prix.messageError}<br />
                                <div className="prixAddAnnonceInput">
                                    <div className="unitePrixAddAnnnonce">€</div>
                                    <input type="number" id="priceEditAnnonce" onKeyUp={(event) => setPrix({ value: event.currentTarget.value })} className="my-form-control" />
                                </div>

                                <label className="myLabel">Etat de stock</label><br />
                                <input type="radio" checked={radioBtn.enStock} name="stock" onClick={() => {
                                    setStock("En stock");
                                    setRadioBtn({
                                        horsStock: false,
                                        enStock: true
                                    })
                                }} id="en_stock" /> <label htmlFor="en_stock" className="myLabel">En stock</label>
                                <input type="radio" checked={radioBtn.horsStock} name="stock" onClick={() => {
                                    setStock("Hors stock");
                                    setRadioBtn({
                                        horsStock: true,
                                        enStock: false
                                    })
                                }} id="hors_stock" /> <label htmlFor="hors_stock" className="myLabel">Hors stock </label><br />

                                <label className="myLabel">Période de disponibilité{requireAsterix}</label>{dispoStatus.message}<br />
                                <div className="w3-row">
                                    <div className="w3-col" style={{ width: "45%" }}>
                                        <div className="w3-row">
                                            <div className="w3-col" style={{ width: "30%" }}>
                                                <label className="dateLabel">Date début</label>
                                            </div>
                                            <div className="w3-col" style={{ width: "70%" }}>
                                                <input type="date" onChange={async (event) => {
                                                    dateKeyUp(event.currentTarget.value, dateFin);
                                                    setDateDebut(event.currentTarget.value)
                                                }} id="dateDebut" className="myInputAddAnnonce" />
                                            </div>
                                        </div>
                                    </div>
                                    <div className="w3-col" style={{ width: "10%" }}>
                                        <div style={{ textAlign: "center", fontSize: 20 }} id="width">-</div>
                                    </div>
                                    <div className="w3-col" style={{ width: "45%" }}>
                                        <div className="w3-row">
                                            <div className="w3-col" style={{ width: "25%" }}>
                                                <label htmlFor="dateFin" className="dateLabel">Date fin</label>
                                            </div>
                                            <div className="w3-col" style={{ width: "75%" }}>
                                                <input type="date" onChange={(event) => {
                                                    dateKeyUp(dateDebut, event.currentTarget.value);
                                                    setdateFin(event.currentTarget.value)
                                                }} id="dateFin" className="myInputAddAnnonce" />
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <label className="myLabel" style={{ marginTop: 20 }}>Photos{requireAsterix}</label>{elementErrorPhotos.messageError.map(error => error)}<br />
                                <div className="photoContainer">
                                    <div className="w3-row">
                                        {photosAnnonce.map((photo, key) => (
                                            <div key={key} className="w3-col s3 imgContainer">
                                                <div className="imageContent bord-dash bord-dash-orange">
                                                    <img src={url + "/images/" + photo} style={{ width: "100%" }} className="imgDrop" alt={"image_" + key} />
                                                </div>
                                                <div className="deletePhotoContainer w3-display-container" style={{ width: "100%", height: "100%" }}>
                                                    <div className="deletePhoto w3-display-bottomright" onClick={() => deleteOldPhoto(photo)}><FontAwesomeIcon style={{ marginTop: 2 }} icon="trash" /></div>
                                                </div>
                                            </div>
                                        ))}
                                        {photos.map((photo, key) => (
                                            <div key={key} className="w3-col s3 imgContainer">
                                                <div className="imageContent bord-dash bord-dash-orange">
                                                    <img src={photo.preview} style={{ width: "100%" }} className="imgDrop" alt={"image_" + key} />
                                                </div>
                                                <div className="deletePhotoContainer w3-display-container" style={{ width: "100%", height: "100%" }}>
                                                    <div className="deletePhoto w3-display-bottomright" onClick={() => deletePhoto(key)}><FontAwesomeIcon style={{ marginTop: 2 }} icon="trash" /></div>
                                                </div>
                                            </div>
                                        ))}
                                        <div className="w3-col s3 imgContainer">
                                            <ReactDropzone
                                                multiple={false}
                                                onDropRejected={(arg) => onDropRejectedFile(arg)}
                                                onDrop={(files) => onDropFile(files)}
                                                accept="image/*">
                                                {({ getRootProps, getInputProps }) => (
                                                    <div {...getRootProps()}>
                                                        <input {...getInputProps()} />
                                                        <div className="w3-display-container imageContent bord-dash bord-dash-gray">
                                                            <div className="w3-display-middle">
                                                                <span style={{
                                                                    fontSize: 40,
                                                                    opacity: 0.5
                                                                }}><FontAwesomeIcon icon="plus-circle" /></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                )}
                                            </ReactDropzone>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div className="cardContainer">
                            <div className="headerCard">
                                Localisation
                            </div>
                            <div className="bodyCard">
                                <div className="googleMapContainer w3-display-container">
                                    <div id="googleMapContent">
                                        <Map getCoord={localisationChoosen} coor={localisationCoor} />
                                    </div>
                                    <div id="searchLocalisationContainer" style={{ zIndex: 100000 }} className="w3-display-topmiddle">
                                        <div className="w3-row">
                                            <div className="w3-col" style={{ width: "7%" }}>
                                                <div className="w3-display-container iconLocalisation">
                                                    <div className="w3-display-middle"><FontAwesomeIcon icon="crosshairs" /></div>
                                                </div>
                                            </div>
                                            <div className="w3-col" style={{ width: "86%" }}>
                                                <AutoComplete labelShow={false} inputClassName={"inputSearchLocalisation"} getValue={localisationChoosen} />
                                            </div>
                                            <div className="w3-col" style={{ width: "7%" }}>
                                                <div className="w3-display-container iconLocalisation" id="iconSearchLocalisation">
                                                    <div className="w3-display-middle"><FontAwesomeIcon icon="search" /></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="cardContainer">
                            <div className="headerCard">
                                Information complémentaires
                            </div>
                            <div className="bodyCard">
                                <label className="myLabel" style={{ marginTop: 20 }}>Email{requireAsterix}</label>{email.messageError}<br />
                                <input type="text" id="emailEditAnnonce" onKeyUp={(event) => emailKeyUp(event.currentTarget.value)} className="my-form-control" />
                                <label className="myLabel" style={{ marginTop: 20 }}>Téléphone{requireAsterix}</label>{tel.messageError}<br />
                                <input type="text" id="telephoneEditAnnonce" onKeyUp={(event) => telKeyUp(event.currentTarget.value)} className="my-form-control" />
                            </div>
                        </div>
                        <div className="conditionContainer" style={{ marginBottom: 40 }}>
                            <p style={{ textAlign: "justify" }}>En validant la diffusion de mon annonce, j'accepte les <a href="" style={{ color: "blue" }}>conditions générales</a> d'utilisation du site inmybush.com et j'autorise inmybush.com à diffuser mon annonce.</p>
                            <p style={{ fontWeight: "bold" }}>{requireAsterix}: Champs obligatoires</p>
                        </div>
                        <div className="w3-display-container">
                            <div className="w3-display-right">
                                <div className="btnAddAnnonceContainer">
                                    <button onClick={() => saveProduit()} className="my-btn-lg c-green">{btnName}</button>&nbsp;&nbsp;
                                    <button onClick={() => window.location.assign("/")} className="my-btn-lg annulerAddAnnonce">Annuler</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="col-lg-2 col-md-2 col-sm-12"></div>
            </div>
        </div>
    );
}