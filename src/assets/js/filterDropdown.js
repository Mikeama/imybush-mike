export default class filterListDropdown {

    static filterList(inputId, dropDownId) {
        var input, filter, a, i;
        input = document.getElementById(inputId);
        filter = input.value.toUpperCase();
        var div = document.getElementById(dropDownId);
        a = div.getElementsByTagName("a");
        for (i = 0; i < a.length; i++) {
            var txtValue = a[i].textContent || a[i].innerText;
            if (txtValue.toUpperCase().indexOf(filter) > -1) {
                a[i].style.display = "";
            } else {
                a[i].style.display = "none";
            }
        }
    }

    static showList(id) {
        document.getElementById(id).classList.toggle("show");
    }
}