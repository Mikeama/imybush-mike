import { library } from "@fortawesome/fontawesome-svg-core";
import { faSearch, faHome, faMapMarker, faPhone, faEnvelope, faAngleLeft, faAngleRight, faChevronDown, faEye, faEyeSlash, faCrosshairs, faPlusCircle, faTrash, faEdit, faTimes, faBars, faBullhorn, faBoxes, faUsersCog, faSignOutAlt, faUser } from "@fortawesome/free-solid-svg-icons";

library.add(faSearch, faHome, faMapMarker, faPhone, faEnvelope, faAngleLeft, faAngleRight, faChevronDown, faEye, faEyeSlash, faCrosshairs, faPlusCircle, faTrash, faEdit, faTimes, faBars, faBullhorn, faBoxes, faUsersCog, faSignOutAlt, faUser);