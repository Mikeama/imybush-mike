export const tempReducer = (state = null, action) => {
    switch(action.type) {
        case 'SET_TEMP':
            return action.payload;
        case 'DELETE_TEMP':
            return null
        default: 
            return state;
    }
}