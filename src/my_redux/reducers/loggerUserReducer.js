export const loggerUserReducer = (state = null, action) => {
    switch(action.type) {
        case 'LOGGING_USER' : 
            return {
                current_user: {
                    id: action.payload.id,
                    username: action.payload.username,
                    token: action.payload.token,
                    name: action.payload.name
                }
            };
        case 'LOGGOUT_USER' :
            return null;
        default :
            return state;
    }
}