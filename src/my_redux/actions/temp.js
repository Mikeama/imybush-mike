export const setTemp = (temp = null) => {
    return {
        type: 'SET_TEMP',
        payload: temp
    }
}

export const deleteTemp = () => {
    return {
        type: 'DELETE_TEMP'
    }
}