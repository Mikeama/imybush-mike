export const loggingUser = (userInformation) => {
    return {
        type: "LOGGING_USER",
        payload: userInformation
    }
}

export const loggoutUser = () => {
    return {
        type: "LOGGOUT_USER"
    }
}