import React, { useEffect } from 'react';
import { usePagination } from '@material-ui/lab/Pagination';
import { makeStyles } from '@material-ui/core/styles';
import "./css/pagination.css";
import $ from "jquery";

const useStyles = makeStyles({
  ul: {
    border: "1px solid rgba(0,0,0, 0.3)",
    borderRadius: 4,
    fontSize: 'small',
    listStyle: 'none',
    padding: 0,
    margin: 0,
    display: 'flex',
  },
});

export default function Pagination(prop) {
  const classes = useStyles();
  const { items } = usePagination({
    count: prop.count,
  });

  useEffect(() => {
    $(document).ready(() => {
      $(".btnDivPagination").css({
        "width": "29px",
        "height": "29px",
        "padding": "15%"
      })
    });
  })

  return (
    <nav>
      <ul className={classes.ul}>
        {items.map(({ page, type, selected, ...item }, index) => {
          let children = null;

          if (type === 'start-ellipsis' || type === 'end-ellipsis') {
            children = <div className="borderRightPagination" style={{ width: 30, textAlign: "center", height: 30, paddingTop: 3 }}>...</div>;
          } else if (type === 'page') {
            children = (
              <button type="button" className="btn-pagination borderRightPagination" style={{ background: selected ? 'rgb(248, 133, 1)' : undefined, color: selected ? 'white' : undefined, borderRight: selected ? '1px solid rgba(0,0,0,0.04)' : undefined }} {...item}>
                <div className="btnDivPagination" onClick={() => prop.onChange(page)}>{page}</div>
              </button>
            );
          } else {
            var className = type === "previous" ? "btn-pagination borderRigthFirst" : "btn-pagination"
            children = (
              <button className={className} type="button" {...item}>
                <div style={{ width: "100%", height: "100%", marginRight: 30 }} onClick={() => prop.onChange(page)}>{type === "next" ? "Suivant" : type === "previous" ? "Précédent" : ""}</div>
              </button>
            );
          }
          return <li key={index}>{children}</li>;
        })}
      </ul>
    </nav>
  );
}
