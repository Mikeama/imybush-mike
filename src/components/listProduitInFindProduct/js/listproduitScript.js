import $ from "jquery";

const Responsive = () => {
    $(document).ready(() => {
        responsive();
        $(window).resize(() => {
            responsive();
        });
        function responsive() {
            var windowWidth = $(window).width();
            if (windowWidth >= 984) {
                $(".goDownFist").css({
                    "margin-top": "0%",
                    "width": "25%",
                    "border-top": "none",
                    "height": "230px",
                    "padding": "0px"
                });
                $(".information2Container").css({
                    "padding": "0px 0px 0px 5%",
                    "border-top": "none",
                });
    
                $(".informtion1Container").css("padding-left", "50px");
    
                $(".goDownSecond").css({
                    "width": "53%",
                    "border-right": "1px solid orange",
                    "height": "230px",
                    "margin-bottom": "0px",
                    "padding": "0"
                });
    
                $(".imageContainer").css({
                    width: "22%"
                });
    
                $(".datePostContainer").css({
                    "left": "50px"
                });
            } else if (windowWidth < 984 && windowWidth >= 653) {
                $(".goDownFist").css({
                    "margin-top": "2%",
                    "width": "100%",
                    "height": "150px",
                    "border-top": "1px solid orange",
                    "padding": "0px"
                });
                $(".information2Container").css({
                    "padding": "2% 0px 0px 1.5%",
                });
                $(".goDownSecond").css({
                    "width": "60%",
                    "border-right": "none",
                    "height": "230px",
                    "margin-bottom": "0px"
                });
                $(".imageContainer").css({
                    width: "40%"
                });
                $(".informtion1Container").css("padding-left", "10px");
                $(".datePostContainer").css({
                    "left": "3%"
                });
            } else if (windowWidth < 653) {
                $(".goDownFist").css({
                    "margin-top": "2%",
                    "width": "100%",
                    "height": "160px",
                    "border-top": "1px solid orange",
                    "padding": "2%"
                });
                $(".goDownSecond").css({
                    "width": "100%",
                    "border-right": "none",
                    "height": "180px",
                    "margin-bottom": "10px",
                    "padding": "15px"
                });
                $(".information2Container").css({
                    "padding": "0",
                });
                $(".informtion1Container").css({
                    "padding-left": "0px",
                    "padding-top": "2%",
                });
                $(".datePostContainer").css({
                    "left": "15px"
                });
                $(".imageContainer").css({
                    width: "100%"
                });
            }
        }
    });
}

export default Responsive;



