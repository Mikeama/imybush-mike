import React, { useEffect, useState } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import responsive from "./js/listproduitScript";
import "./js/listproduitScript";
import "./css/listProduit.css";

export default function ListPRoduit(prop) {

    const [image, setImage] = useState("");
    const [name, setName] = useState("");
    const [prix, setPrix] = useState("");
    const [dateDebut, setDateDebut] = useState("");
    const [dateFin, setDateFin] = useState("");
    const [adresse, setAdresse] = useState("");
    const [phone, setPhone] = useState("");
    const [nameVendeur, setNameVendeur] = useState("");
    const [datePost, setDatePost] = useState("");
    const [email, setEmail] = useState("");

    useEffect(() => {
        setImage(prop.data.image);
        setName(prop.data.name);
        setPrix(prop.data.prix);
        setDateDebut(prop.data.dateDebut);
        setDateFin(prop.data.dateFin);
        setAdresse(prop.data.adresse);
        setPhone(prop.data.phone);
        setNameVendeur(prop.data.nameVendeur);
        setDatePost(prop.data.datePost);
        setEmail(prop.data.email);
        responsive(); // responive function 
    }, [prop.data.name, prop.data.prix, prop.data.dateDebut, prop.data.adresse, prop.data.phone, prop.data.dateFin, prop.data.image, prop.data.nameVendeur, prop.data.datePost, prop.data.email])

    return (
        <div className="listProduitContainerFindProduct" style={{ width: "100%", marginBottom: 30 }}>
            <div className="w3-row ListProduitConatentFindProduct">
                <div className="w3-col imageContainer">
                    <img src={image} alt={name} className="imgFindProduct" />
                </div>
                <div className="w3-col goDownSecond" style={{ position: "relative" }}>
                    <div className="informtion1Container">
                        <h2 className="text-overflow" style={{ fontSize: "x-large", fontWeight: 600, margin: 0, width: 300 }}>{name}</h2>
                        <div className="prixContentFindProduct">{prix}€</div>
                        <div className="dispoContentFindProduit" style={{ marginTop: 10 }}>
                            <label style={{ fontSize: "small" }}>Période de disponibilité</label>
                            <div style={{ marginTop: 10, fontSize: "small" }}><span className="dateDispo">{dateDebut}</span>&nbsp;jusqu'au&nbsp;<span className="dateDispo">{dateFin}</span></div>
                        </div>
                    </div>
                    <div className="datePostContainer">{datePost}</div>
                </div>
                <div className="w3-col myColFindProduct goDownFist">
                    <div className="information2Container" style={{ width: "100%", height: "100%" }}>
                        <h2 style={{ fontSize: "large", margin: 0, fontWeight: 500, marginBottom: 5 }}>Posté par :</h2>
                        <h2 style={{ fontSize: "large", color: "orange", margin: 0, marginBottom: 20, fontWeight: 500 }}>{nameVendeur}</h2>
                        <div className="information2 text-overflow" style={{ width: 200 }}><FontAwesomeIcon icon="home" />&nbsp;{adresse}</div>
                        <div className="information2 text-overflow" style={{ width: 200 }}><FontAwesomeIcon icon="phone" />&nbsp;{phone}</div>
                        <div className="information2 text-overflow" style={{ width: 200 }}><FontAwesomeIcon icon="envelope" />&nbsp;{email}</div>
                    </div>
                </div>
            </div>
        </div>
    );
}