import React, { useState, useEffect } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import defaultAvatar from "../../assets/icon/avatar_default1.png";
import "./css/informationUser.css";
import $ from "jquery";

export default function InformationUser(prop) {

    const [name, setName] = useState("");
    const [photoUser, setPhotoUser] = useState("");
    const [adresse, setAdresse] = useState("");
    const [phone, setPhone] = useState("");
    const [email, setEmail] = useState("");

    useEffect(() => {
        setName(prop.name);
        setPhone(prop.phone);
        setAdresse(prop.adresse);
        setEmail(prop.email);
        $(document).ready(() => {
            $.get(prop.photoUser).fail(() => {
                setPhotoUser(defaultAvatar);
            }).done((data, status, req) => {
                if (req.getResponseHeader('content-type').indexOf("image/") === -1) {
                    setPhotoUser(defaultAvatar);
                } else {
                    setPhotoUser(data)
                }
            });
        });
    }, [prop.name, prop.photoUser, prop.phone, prop.adresse, prop.email]);

    return (
        <div className="informationUser">
            <div className="headerContentInformationUser">
                <h2 style={{
                    margin: 0,
                    fontSize: "medium",
                    fontWeight: "bold",
                    color: "#ffffff"
                }}>INFORMATIONS</h2>
            </div>
            <div className="bodyContentInformationUser">
                <div className="imageAndNom w3-display-container" style={{ width: "100%" }}>
                    <div className="w3-display-middle" style={{ textAlign: "center" }}>
                        <div className="imageContentInformationUser" style={{ marginBottom: 5 }}>
                            <img src={photoUser} className="imageInformationUser" alt={name} />
                        </div>
                        <div className="nameContentInformationUser">{name}</div>
                    </div>
                </div>
                <div className="row" style={{ marginTop: 125, marginBottom: 10 }}>
                    <div className="col-sm-7 addressTelEmail">
                        <div><FontAwesomeIcon icon="home" />&nbsp;{adresse}</div>
                        <div><FontAwesomeIcon icon="phone" />&nbsp;{phone}</div>
                        <div><FontAwesomeIcon icon="envelope" />&nbsp;{email}</div>
                    </div>
                    <div className="col-sm-5" style={{ paddingTop: 20 }}>
                        <button className="callInformationUser c-green">Appeler</button>
                    </div>
                </div>
                <div className="row" style={{ display: prop.hide ? "none" : "block" }}>
                    <div className="col-sm-3"></div>
                    <div className="col-sm-6">
                        <button onClick={() => window.location.assign("/front/vendeur_produits/1")} className="getAllProduitUser">Tous ses produits</button>
                    </div>
                    <div className="col-sm-3"></div>
                </div>
            </div>
        </div>
    );
}