import React from "react";

export default class ResponsiveGrid extends React.Component {

    state = {
        row: this.props.row,
        col: this.props.col,
        data: this.props.data,
        componentBuilt: []
    }

    buildGrid = (row, col, data) => {
        var Row = [];
        var k = 0;
        for (var i = 1; i <= row; i++) {
            var Col = [];
            for (var j = 1; j <= ((col * 2) - 1); j++) {
                if (j % 2 === 0) {
                    Col.push(<div key={j} className="w3-col" style={{ width: data.spaceWidthInPercent + "%", opacity: 0 }}>0</div>);
                } else {
                    var width = (100 - (data.spaceWidthInPercent * (col - 1))) / col;
                    Col.push(<div key={j} className="w3-col" style={{ width: width + "%" }}>{(k < data.contents.length) ? data.contents[k] : null}</div>);
                    k++;
                }
            }
            Row.push(<div key={i} className="w3-col">{Col.map((item, key) => (item))}</div>);
        }
        this.setState({
            componentBuilt: Row
        });
    }

    componentDidMount() {
        this.buildGrid(this.state.row, this.state.col, this.state.data);
    }
    render() {

        return (
            <div className="responsiveGrid" style={{
                width: "100%"
            }}>
                {this.state.componentBuilt.map((row, key) => (row))}
            </div>
        );
    }

}