import React, { useState, useEffect } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import "./css/vendeur.css";
import $ from "jquery";
import avatarDefault from "../../assets/icon/avatar_default1.png";

export default function Vendeur(prop) {

    const [pdp, setPdp] = useState(avatarDefault);
    const [name, setName] = useState("");
    const [produit, setProduit] = useState("");
    const [adresse, setAdresse] = useState("");
    const [date, setDate] = useState("");

    useEffect(() => {
        setName(prop.name);
        setProduit(prop.produit);
        setAdresse(prop.adresse);
        setDate(prop.date);
        $(document).ready(() => {
            //test image
            $.get(prop.pdp)
                .done((data, statustext, req) => {
                    if(req.getResponseHeader('content-type').indexOf("image/") === -1) {
                        setPdp(avatarDefault);
                    } else {
                        setPdp(data);
                    }
                }).fail(() => {
                    setPdp(avatarDefault);
                })
        });
    }, [prop.name, prop.produit, prop.adresse, prop.date, prop.pdp]);

    return (
        <div id="vendeur">
            <div className="vendeurContainer">
                <img src={pdp} alt="Avatar" style={{ width: 60 }} />
                <div id="name" style={{ color: "rgb(248, 133, 1)" }}>{name}</div>
                <div id="produit"><span style={{ color: "rgb(120, 120, 120)" }}>posté:</span> {produit}</div>
                <div id="addresseAndDate">
                    <div id="adresse">
                        <FontAwesomeIcon style={{ fontSize: 12 }} icon="home" /> {adresse}
                    </div>
                    <div id="date">
                        {date}
                    </div>
                </div>
            </div>
        </div>
    )
}
