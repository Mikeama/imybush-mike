export default class Fonction {
    //Création date
    changeDate = (date) => {
        let tab = date.split('/');
        let mois = '';
        switch (tab[1]){
            case '1': 
                mois = 'Janvier';
                break;
            case '2': 
                mois = 'Fevrier';
                break;
            case '3': 
                mois = 'Mars';
                break;
            case '4': 
                mois = 'Avril';
                break;
            case '5': 
                mois = 'Mai';
                break;
            case '6': 
                mois = 'Juin';
                break;
            case '7': 
                mois = 'Juillet';
                break;
            case '8': 
                mois = 'Août';
                break;
            case '9': 
                mois = 'Septembre';
                break;
            case '10': 
                mois = 'Octobre';
                break;
            case '11': 
                mois = 'Novembre';
                break;
            default: 
                mois = 'Décembre';
        }

        return tab[0]+' '+mois+' '+tab[2];
    }
}