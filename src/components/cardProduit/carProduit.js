import React, {useState} from "react";
import "./css/card.css";

export default function CardProduit(prop) {

    const [imageOrPlaceholder] = useState(prop.imageOrPlaceholder);
    const [altImage] = useState(prop.altImage);
    const [imageLazyload] = useState(prop.imageLazyload);
    const [nameProduit] = useState(prop.nameProduit);
    const [prixProduit] = useState(prop.prixProduit);


    return (
        <div onClick={() => window.location.assign("/front/detail_produit/1")} className="my-card" style={prop.style}>
            <div className="imgContent">
                <img className="img" src={imageOrPlaceholder} alt={altImage} data-flickity-lazyload={imageLazyload ? imageLazyload : null } />
            </div>
            <div className="bodyContent">
                <div className="nameContent">
                    {nameProduit}
                </div>
                <div className="priceContent">
                    {prixProduit}€
                </div>
            </div>
        </div>
    );
}