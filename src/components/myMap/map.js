import React from "react";
import L from "leaflet";
import $ from "jquery";

const style = {
  width: "100%",
  height: "300px"
};

class Map extends React.Component {

  state = {
    latCenter: this.props.markerPosition.lat,
    lngCenter: this.props.markerPosition.lng
  }

  componentDidMount() {
    // create map
    this.map = L.map("map", {
      center: [this.state.latCenter, this.state.lngCenter],
      zoom: 16,
      layers: [
        L.tileLayer("http://{s}.tile.osm.org/{z}/{x}/{y}.png")
      ],
      zoomControl: false
    });

    var greenIcon = L.icon({
      iconUrl: '/image/marker.png',
      iconSize:     [38, 50], // size of the icon
      shadowSize:   [50, 64], // size of the shadow
      iconAnchor:   [22, 40], // point of the icon which will correspond to marker's location
      shadowAnchor: [4, 62],  // the same for the shadow
      popupAnchor:  [-3, -76] // point from which the popup should open relative to the iconAnchor
  });
    // add marker
    this.marker = L.marker(this.props.markerPosition, { icon: greenIcon }).addTo(this.map);
    L.control.zoom({
      position: 'bottomright'
    }).addTo(this.map);

    $('.leaflet-container').css('cursor','crosshair');

    this.map.addEventListener("click", (event) => {
      this.props.getCurrentMarker({
        lat: event.latlng.lat,
        lng: event.latlng.lng
      });
    });
  }
  componentDidUpdate({ markerPosition }) {
    if (this.props.markerPosition !== markerPosition) {
      this.marker.setLatLng(this.props.markerPosition);
      this.map.setView(new L.LatLng(this.props.markerPosition.lat, this.props.markerPosition.lng), 14);
    }
  }
  render() {
    return <div id="map" style={style} />;
  }
}

export default Map;
