import React, { useState, useEffect } from 'react';
import Map from './map';

export default function MyMap(props) {

    const [markerPosition, setMarkerPosition] = useState({
        lat: 16.264361,
        lng: -61.490371
    });

    const getCurrentMarker = (coord) => {
        setMarkerPosition({
            lat: coord.lat,
            lng: coord.lng,
        });
        props.getCoord(coord);
    };

    useEffect(() => {
        if (props.coor !== null) {
            if (props.coor.hasOwnProperty('lat')) {
                setMarkerPosition({
                    lat: props.coor.lat,
                    lng: props.coor.lng
                })
            } else {
                setMarkerPosition({
                    lat: props.coor.coordinates[1],
                    lng: props.coor.coordinates[0]
                })
            }
        }
    }, [props.coor]);

    return (
        <>
            <Map markerPosition={markerPosition} getCurrentMarker={getCurrentMarker} />
        </>
    )
}