import React, { Component } from 'react';
import { Map, GoogleApiWrapper, InfoWindow, Marker, Polygon } from 'google-maps-react';
import villes from '../../data/ville';

const mapStyles = {
	width: "100%",
	height: "100%"
};

const VillesList = Object.keys(villes);

export class MapContainer extends Component {

	state = {
		zoom: 12,
		showingInfoWindow: false,
		activeMarker: {},
		selectedPlace: {},
		polygone: []
	};

	onMarkerClick = (props, marker, e) => {

		this.setState({
			polygone: []
		});

		const polygones = villes[props.name]['fields']['geo_shape']['coordinates'];

		const polyContent = [];
		var i = 0;
		for (const polygone of polygones) {
			const data = [];
			for (const coordinate of polygone) {
				var object = {};
				object["lat"] = coordinate[1]
				object["lng"] = coordinate[0]
				data.push(object);
			}
			polyContent.push(
				<Polygon
					key={i}
					path={data}
					options={{
						strokeColor: "#33ccff",
						strokeOpacity: 0.5,
						strokeWeight: 1,
						fillColor: "#33ccff",
						fillOpacity: 0.1
					}}
				/>
			)
			i++;
		}

		this.setState({
			selectedPlace: props,
			activeMarker: marker,
			showingInfoWindow: true,
			polygone: polyContent
		});
	}

	onClose = props => {
		if (this.state.showingInfoWindow) {
			this.setState({
				showingInfoWindow: false,
				activeMarker: null
			});
		}
	};

	render() {
		return (
			<Map
				google={this.props.google}
				zoom={this.state.zoom}
				style={mapStyles}
				initialCenter={
					{
						lat: 16.264361,
						lng: -61.490371
					}
				}
			>
				{
					VillesList.map((VilleName, key) => (
						<Marker
							key={key}
							position={{
								lat: parseFloat(villes[VilleName]['fields']['geo_point_2d'][0]),
								lng: parseFloat(villes[VilleName]['fields']['geo_point_2d'][1])
							}}
							onClick={this.onMarkerClick}
							name={VilleName}
						/>
					))
				}
				{
					this.state.polygone.map(item => (item))
				}
				<InfoWindow
					marker={this.state.activeMarker}
					visible={this.state.showingInfoWindow}
					onClose={this.onClose}
				>
					<div>
						<h6>{this.state.selectedPlace.name}</h6>
					</div>
				</InfoWindow>
			</Map>
		);
	}
}

export default GoogleApiWrapper({
	apiKey: 'AIzaSyA4S2hLwJIH5A4EHwcZoEOhs0IvqEIopzM'
})(MapContainer);