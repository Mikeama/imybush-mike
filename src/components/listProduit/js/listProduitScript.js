import $ from "jquery";

$(document).ready(() => {
    responsive();
    $(window).resize(() => {
        responsive()
    });
    function responsive() {
        var windowWidth = $(window).width();
        if (windowWidth >= 984) {
            $(".informationProduitList").css("padding-left", "5%");
            $(".informationProduitListGrid").css({
                "width": "59%",
                "border-right": "1px solid rgb(248, 133, 1)"
            });
            $(".imageGridListProduit").css("width", "20%");
            $(".statusContainer").css("width", "21%");
            $(".statusListeProduitContainer").css({
                "width": "85%",
                "margin-left": "11%"
            });
            $(".statusContainer").css({
                "width": "21%"
            })
            $(".imgListProduit").css({
                "width": "100%",
                "height": "220px"
            });
            $(".dateDispoContainer").css("font-size", "small");
            $(".under").css("padding-top", "0px");
        } else if (windowWidth >= 704 && windowWidth < 984) {
            $(".informationProduitList").css("padding-left", "10px");
            $(".informationProduitListGrid").css({
                "width": "52%",
                "border-right": "1px solid rgb(248, 133, 1)"
            });
            $(".imageGridListProduit").css("width", "26%");
            $(".statusContainer").css("width", "21%");
            $(".imgListProduit").css({
                "width": "100%",
                "height": "220px"
            });
            $(".statusListeProduitContainer").css({
                "width": "85%",
                "margin-left": "11%"
            });
            $(".dateDispoContainer").css("font-size", "small");
            $(".under").css("padding-top", "0px");
        } else if (windowWidth < 704) {
            $(".imageGridListProduit").css({ "width": "100%" });
            $(".informationProduitListGrid").css({
                "width": "100%",
                "border-right": "none"
            });
            $(".imgListProduit").css({
                "width": "100%",
                "height": $(".imgListProduit").width()
            });
            $(".statusContainer").css("width", "100%");
            $(".under").css("padding-top", "2%");
            $(".statusListeProduitContainer").css({
                "width": "100%",
                "margin-left": "0"
            });
        }
    }
});


