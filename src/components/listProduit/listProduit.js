import React, { useState, useEffect } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import "./css/listProduit.css";
import "./js/listProduitScript";

export default function ListProduit(prop) {

    const [image, setImage] = useState("");
    const [name, setName] = useState("");
    const [prix, setPrix] = useState("");
    const [dateDebut, setDateDebut] = useState("");
    const [dateFin, setDateFin] = useState("");
    const [adresse, setAdresse] = useState("");
    const [phone, setPhone] = useState("");
    const [status, setStatus] = useState({
        statusValue: "",
        statusText: ""
    });

    useEffect(() => {
        setImage(prop.data.image);
        setName(prop.data.name);
        setPrix(prop.data.prix);
        setDateDebut(prop.data.dateDebut);
        setDateFin(prop.data.dateFin);
        setAdresse(prop.data.adresse);
        setPhone(prop.data.phone);
        setStatus({
            statusText: prop.data.status.statusText,
            statusValue: prop.data.status.statusValue
        });
    }, [prop.data.name, prop.data.prix, prop.data.dateDebut, prop.data.adresse, prop.data.phone, prop.data.status, prop.dateFin, prop.data.image, prop.data.dateFin])

    return (
        <div onClick={() => window.location.assign("/detail_produit/1")} className="containerListProduit">
            <div className="w3-row contentlistProduit " style={{ width: "100%", fontSize: "small" }}>
                <div className="w3-col responiseListProduit imageGridListProduit" style={{ width: "20%" }}>
                    <img className="imgListProduit" src={image} alt={name} />
                </div>
                <div className="w3-col responiseListProduit under informationProduitListGrid" style={{ width: "59%", borderRight: "1px solid orange" }}>
                    <div className="informationProduitList">
                        <h2 className="produitNameListProduit">{name}</h2>
                        <div className="prixProduitListProduit">{prix}€</div>
                        <label style={{ fontSize: "small" }}>Période de disponibilité</label><br />
                        <div className="dateDispoContainer" style={{ marginBottom: 25 }}><span className="dateDispo">{dateDebut}</span>&nbsp;jusqu'au&nbsp;<span className="dateDispo">{dateFin}</span></div>
                        <div><FontAwesomeIcon icon="home" />&nbsp;{adresse}</div>
                        <div><FontAwesomeIcon icon="phone" />&nbsp;{phone}</div>
                    </div>
                </div>
                <div className="w3-col under statusContainer" style={{ width: "21%" }}>
                    <div className="statusListeProduitContainer">
                        <div className={"statusListeProduit " + status.statusValue}>{status.statusText}</div>
                    </div>
                </div>
            </div>
        </div>
    );
}

