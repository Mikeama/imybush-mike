/* eslint-disable no-use-before-define */
import React, { useState } from 'react';
import useAutocomplete from '@material-ui/lab/useAutocomplete';
import { makeStyles } from '@material-ui/core/styles';
import Axios from 'axios';

const useStyles = makeStyles((theme) => ({
    listbox: {
        width: "86%",
        margin: 0,
        padding: 0,
        zIndex: 1,
        position: 'absolute',
        listStyle: 'none',
        backgroundColor: theme.palette.background.paper,
        overflow: 'auto',
        maxHeight: 200,
        border: '1px solid rgba(0,0,0,.25)',
        '& li[data-focus="true"]': {
            backgroundColor: '#4a8df6',
            color: 'white',
            cursor: 'pointer',
        },
        '& li:active': {
            backgroundColor: '#2977f5',
            color: 'white',
        },
    },
}));

export default function UseAutocomplete(props) {

    const classes = useStyles();

    const [regions, setRegions] = useState([]);
    const getValue = (event, value) => {
        props.getValue(value);
    }

    const {
        getRootProps,
        getInputLabelProps,
        getInputProps,
        getListboxProps,
        getOptionProps,
        groupedOptions
    } = useAutocomplete({
        id: 'use-autocomplete-demo',
        options: regions,
        onChange: getValue,
        getOptionLabel: (option) => option.title,
    });

    const setAutoCompleteData = (searchIndex) => {
        if (searchIndex !== "" && searchIndex) {
            Axios.get("https://api-adresse.data.gouv.fr/search/?q=" + searchIndex + "&type=street&limit=30").then(res => {
                const features = res.data.features;
                const autocompleteData = [];
                for (const item of features) {
                    if (item.properties.context.split(",")[0] === "971") {
                        autocompleteData.push({
                            title: item.properties.label,
                            coordinates: item.geometry.coordinates
                        })
                    }
                }
                setRegions(autocompleteData);
            }).catch(err => {
                alert(err.message);
            })
        }
    }

    return (
        <>
            <div {...getRootProps()}>
                {props.labelShow &&
                    <label className={props.labelClasseName} {...getInputLabelProps()}>
                        {props.labelContent}
                    </label>
                }
                <input onKeyUp={(event) => setAutoCompleteData(event.currentTarget.value)} className={props.inputClassName} {...getInputProps()} />
            </div>
            {groupedOptions.length > 0 ? (
                <ul className={classes.listbox} {...getListboxProps()}>
                    {groupedOptions.map((option, index) => (
                        <li {...getOptionProps({ option, index })}>{option.title}</li>
                    ))}
                </ul>
            ) : null}
        </>
    );
}


